const spocDetails = require("../models/spocDetails");

exports.saveSpoc = async (data) => {
  try {
    let resps = await new spocDetails(data).save();
    if (resps != null) {
      return resps;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in Save saveSpoc dut to :" + err);
    return null;
  }
};

exports.getSpoc = async (patient_Id) => {
  try {
    let result = await spocDetails.find({ patientId: patient_Id }).sort({ createdDate: -1 }).lean();
    if (result != null) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in getSpoc dut to :" + err);
    return null;
  }
};

