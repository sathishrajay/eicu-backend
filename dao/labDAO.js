const labModel = require("../models/Lab");

exports.saveLabs = async (data) => {
  try {
    let labRes = await new labModel(data).save();
    if (labRes != null) {
      return labRes;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in Save labDAO dut to :" + err);
    return null;
  }
};

exports.getLabs = async (patient_Id) => {
  try {
    let result = await labModel.find({ patientId: patient_Id });
    if (result != null) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in getLabs dut to :" + err);
    return null;
  }
};

exports.update = async (filter, updateReq) => {
  try {
    let resp = await labModel.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};




exports.getTrendingData = async (data) => {
  console.log("Entered getTrendingData-----");
  try {
    let projection = {};

    projection[data.type] = 1;
    projection["date"] = 1;
    projection["_id"] = 0;
    console.log("projection====", projection);
    /* {
      "patientId": "5fa7ee6bc6114d34b43c59e8",  
      "type": "haemoglobin",  
      "fromdate": "2020-12-07",  
      "todate": "2020-12-10"
    } */    

    let result = await labModel.find(
      {
        patientId: data.patientId,
        date: { $gt: new Date(data.fromdate), $lt: new Date(data.todate) },
      },
      projection
    );
    console.log("getTrendingData result in labDAO-----",JSON.stringify(result));
    if (result != null) {
      let data=JSON.parse(JSON.stringify(result));    

    let final_result=[];

     for(var obj in data){      
     
      console.log('json_obj keys is--------',Object.keys(data[obj])); 
       let key0=Object.keys(data[obj])[0];//date tag
       let key1=Object.keys(data[obj])[1];//selected column(haemoglobin,platelates....)          
       const resul_obj={date:data[obj][key0],value:data[obj][key1]};
       final_result.push(resul_obj);    
     }
     console.log('final_result========='+JSON.stringify(final_result));
      return final_result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in getTrendingData dut to :" + err);
    return null;
  }
};
