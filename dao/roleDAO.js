const roleModel = require("../models/role_management");
const mongoose = require("mongoose");

exports.update = async (filter, updateReq) => {
  try {
    let resp = await roleModel.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};

exports.save = async (data) => {
  try {
    let user = await new roleModel(data).save();
    if (user != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.log("error in role "+err);
    return false;
  }
};
exports.findAll = async () => {
  try {
      let result = await roleModel.find();
      if (result) {
          return result;
      } else {
          return null;
      }
  } catch (err) {
      console.log('error in userDAO  fetch is----' + err);
      logger.error('error in userDAO  fetch is----' + err);
      return null;
  }
};

exports.getRoleMenu = async () => {
  try {
    let result = await roleModel.find(
      { roleName: { $ne: "" } , isActive:true},
      "roleName"
    );
      
      if (result) {
          return result;
      } else {
          return null;
      }
  } catch (err) {
      console.log('error in getRoleMenu  fetch is----' + err);
      logger.error('error in getRoleMenu  fetch is----' + err);
      return null;
  }
};

exports.getRoleById = async (roleId) => {
  try {
    let result = await roleModel.findOne(
      { _id: mongoose.Types.ObjectId(roleId) }
    );
      
      if (result) {
          return result;
      } else {
          return null;
      }
  } catch (err) {
      console.log('error in getRoleById  fetch is----' + err);
      logger.error('error in getRoleById  fetch is----' + err);
      return null;
  }
};
