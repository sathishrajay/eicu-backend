const userModel = require("../models/user");

exports.validateUser = async (userName) => {
  try {
    let user = await userModel.findOne(
      { userName: userName },
      "userName password email isActive firstName lastName roleId isVerified"
    );
    if (user) {
      return user;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};

exports.getUserForEmail = async (email) => {
  try {
    let user = await userModel.findOne({ email: email });
    if (user) {
      return user;
    } else {
      return null;
    }
  } catch (err) {
    console.log("error in getUserForEmail user due to "+ err);
    return null;
  }
};

exports.update = async (filter, updateReq) => {
  try {
    let resp = await userModel.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.log("error in update user due to "+ err);
    return false;
  }
};

exports.save = async (data) => {
  try {
    let user = await new userModel(data).save();
    if (user != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.log("error in save user due to "+ err);
    return false;
  }
};

exports.findAll = async () => {
  try {
      let result = await userModel.find();
      if (result) {
          return result;
      } else {
          return null;
      }
  } catch (err) {
      console.log('error in userDAO  fetch is----' + err);
      logger.error('error in userDAO  fetch is----' + err);
      return null;
  }
};
exports.getUserForId= async (id) => {
  try {
    let user = await userModel.findOne({ _id: id });
    if (user) {
      return user;
    } else {
      return null;
    }
  } catch (err) {
    console.log("error in getUserForId user due to "+ err);
    return null;
  }
};

exports.getUserDataForIds = async (userids) => {
  try {
    let result = await userModel.find({
      _id: {
        $in: userids,
      },
    },"userName password email isActive firstName lastName roleId isVerified");
    if (result) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};