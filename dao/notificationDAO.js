const notifications = require("../models/notifications");

/**
 * 
 * @param {*} data  
 * @description:Save notifications
 * @author:Rambabu
 * @date:03-28-2021
 */
exports.saveNotification = async (data) => {
  try {
    let resps = await new notifications(data).save();
    if (resps != null) {
      return resps;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in Save saveNotification dut to :" + err);
    return null;
  }
};

 
 
/**
 * 
 * @param {*} user_Id 
 * @description:Get notifications based on logged in uerid
 * @author:Rambabu
 * @date:03-28-2021
 */
exports.getNotification = async (user_Id) => {
  try {
    let result = await notifications.find({ fk_user_id: user_Id });
    if (result != null) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in getNotification dut to :" + err);
    return null;
  }
};


//filter notification id(primarykey), updateReq--isactive,readstatus,
//{id:notificationid,isactive:true,readstatus:true}

/**
 * 
 * @param {*} user_Id 
 * @description:update notifications based on notification id
 * @author:Rambabu
 * @date:03-28-2021
 */
exports.updateNotification = async (filter, updateReq) => {
  try {
    let resp = await notifications.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.log('Error in updatenotifications is-----',err);
    return false;
  }
};

