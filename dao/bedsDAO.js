const bedsModel = require("../models/beds");

exports.update = async (filter, updateReq) => {
  try {
    let resp = await bedsModel.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};

exports.save = async (data) => {
  try {
    let beds = await new bedsModel(data).save();
    if (beds != null) {
      return beds;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};

exports.getBedForHospitalId = async (hospitalId, bedNum) => {
  try {
    let beds = await bedsModel.findOne({
      fk_hosp_id: hospitalId,
      bed_Num: bedNum,
    });
    if (beds) {
      return beds;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};

exports.getPatientForBed = async (hospital_id, bedNum) => {


  //console.log('commonDAO panameSearch request is-----' + JSON.stringify(req));
  try {

      const result = await bedsModel.aggregate([
            {
              $project: {
                _id: 1,
                bed_Num:1,
                status:1,
                fk_hosp_id:1,
                fk_patient_id: { $toObjectId: "$fk_patient_id" },                  
              },
            },
       
          {
            $lookup: {
              from: "Patient",
              localField: "fk_patient_id",
              foreignField: "_id",
              as: "Patient",
            },
          },

          { $unwind: "$Patient" },
    {
      $project: {
          _id: 1,
          bed_Num:1,
          status:1,
          fk_hosp_id:1,
          name:"$Patient.name",   
      },
    },

    
    {
      $match: {
        bed_Num:bedNum,
        fk_hosp_id:hospital_id
      },
    },
  ]); 
  if(result.length > 0){
    console.log('test data ==================' + JSON.stringify(result))

  }
   if (result) {
          return result;
      } else {
          return null;
      }
  } catch (err) {
      console.log('error in bedsDAO getPatientForBed call----' + err);        
      return null;
  }
};


/**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:07-11-2020
   * @description: Decharge Patient based on hospital_id and bed_num
   */
exports.dischargePatient = async (filter, updateReq) => {
  console.log('dischargePatient in beds dao filter is---'+JSON.stringify(filter))
  console.log('dischargePatient in beds dao updateReq is---'+JSON.stringify(updateReq))
  try {
    let resp = await bedsModel.findOneAndUpdate(filter, updateReq);
    console.log('dischargePatient update response is----'+JSON.stringify(resp));
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};

/**
 * @author:Rambabu.K
 * @date:06-10-2020
 * @description:fetchHospital hospital data state wise
 * @param {*} document 
 */
exports.fetchBeds = async () => {
  console.log('Entered bedsDAO fetchBeds-----');
  try {
     // let result = await hospital.find();
      let result = await bedsModel.find({ status: "occupy" });
      
     // console.log('hopsitalDAO FetchHospital result-------' + JSON.stringify(result))
      if (result) {
          return result;
      } else {
          return null;
      }
  } catch (err) {
      console.log('error in bedsDAO fetchBeds fetch is----' + err);
      logger.error('error in bedsDAO fetchBeds is----' + err);
      return null;
  }
};

exports.getAllBedsForHospitalId = async (hospitalId) => {
  try {
    let beds = await bedsModel.find({
      fk_hosp_id: hospitalId,
    });
    if (beds) {
      return beds;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};