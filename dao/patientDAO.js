const patientModel = require("../models/patient");
const opdHistotyModel = require("../models/opdHistoryModel");

exports.update = async (filter, updateReq) => {
  try {
    let resp = await patientModel.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};

exports.save = async (data) => {
  try {
    let patient = await new patientModel(data).save();
    if (patient != null) {
      return patient;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in Save PatientDAO dut to :" + err);
    return null;
  }
};

exports.saveOpdHistory = async (data) => {
  try {
    let opdHistry = await new opdHistotyModel(data).save();
    if (opdHistry != null) {
      return opdHistry;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in saveOpdHistory PatientDAO due to :" + err);
    return null;
  }
};


/**
 * @author:Rambabu.K
 * @date:08-10-2020
 * @description:Fetch Pateinet Details
 * @param {*} pateintid 
 */
exports.getpatientDetails = async (pateintid) => {
  try {
    let result = await patientModel.findOne({
      _id: pateintid,
    }).lean();
    if (result) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};

/**
 * @author:Varma
 * @date:08-10-2020
 * @description:Fetch Opd History
 * @param {*} pateintid 
 */
 exports.fetchOpdHistory = async (pateintid) => {
  try {
    let result = await opdHistotyModel.find({
      patient_id: pateintid,
    },
    "_id createdDate")
    .sort({ createdDate: -1 }).lean();;
    if (result) {
      return result;
    } else {
      return [];
    }
  } catch (err) {
    return [];
  }
};

exports.retrievespecficOPDhistory = async (id) => {
  try {
    let result = await opdHistotyModel.findOne({ _id: id });
    if (result) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("error in retrievespecficOPDhistory is----" + err);
    return null;
  }
};


/**
 * 
 * @param {*} filter 
 * @param {*} updateReq
 * @author:Rambabu.K
 * @date:07-11-2020
 * @description:Update Patient Status based on patient_id
 */
exports.updatePatientStatus = async (filter, updateReq) => {
  try {
    let resp = await patientModel.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};

/**
 * @author:Rambabu.K
 * @date:08-10-2020
 * @description:Fetch Pateinet Details
 * @param {*} pateintid 
 */
exports.getPatientsForIds = async (pateintids) => {
  try {
    let result = await patientModel.find({
      _id: {
        $in: pateintids,
      },
    },"bed_id name mrNum");
    if (result) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};


/**
 * @author:Rambabu.K
 * @date:28-03-2021
 * @description:Fetch All not equal Discharge Pateinet Details  
 */
exports.getActivePatientDetails = async () => {
  try {
    let result = await patientModel.find({status:{ $ne: "Discharge" }});
    if (result) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};

/**
 * @author:Rambabu.K
 * @date:28-03-2021
 * @description:Fetch All not equal Discharge Pateinet Details  
 */
exports.getPatientDetls = async () => {
  try {
    let result = await patientModel.aggregate([
      
      {
        $project: {
          _id: 1,
          name: 1,
          age: 1,
          sex: 1,
          mrNum: 1,
          status:1,
          bed_id: { $toObjectId: "$bed_id" },
        },
      },

      {
        $lookup: {
          from: "Beds",
          localField: "bed_id",
          foreignField: "_id",
          as: "beds",
        },
      },

      { $unwind: "$beds" },
      {
        $project: {
          name: 1,
          age: 1,
          sex: 1,
          mrNum: 1,
          status:1,
          fk_hosp_id: { $toObjectId: "$beds.fk_hosp_id" },
          bed_Num: "$beds.bed_Num",
        },
      },

      {
        $lookup: {
          from: "Hospital",
          localField: "fk_hosp_id",
          foreignField: "_id",
          as: "hospital",
        },
      },

      { $unwind: "$hospital" },

      {
        $project: {
          name: 1,
          age: 1,
          sex: 1,
          mrNum: 1,
          status:1,
          bed_Num: 1,
          hospital_name: "$hospital.name",
          hospital_code: "$hospital.code",
        },
      },

      {
        $match: {
          status:{ $ne: "Discharge" }
        },
      },
    ]);
if (result) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};

