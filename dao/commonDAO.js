const hospital = require("../models/hospital");
const patient = require("../models/patient");

/**
 * @author:Rambabu.K
 * @date:01-10-2020
 * @description:save hospital data
 * @param {*} document 
 */
exports.panameSearch = async (req) => {


    console.log('commonDAO panameSearch request is-----' + JSON.stringify(req));
    try {
        //let result = await patient.findOne({name: req.name});

        const result = await patient.aggregate([
          /*  {
                $match: {
                  name: req.name,
                },
              }, */

          {
            $project: {
              _id: 1,
              name: 1,
              age: 1,
              sex: 1,
              mrNum: 1,
              status:1,
              bed_id: { $toObjectId: "$bed_id" },
            },
          },

          {
            $lookup: {
              from: "Beds",
              localField: "bed_id",
              foreignField: "_id",
              as: "beds",
            },
          },

          { $unwind: "$beds" },
          {
            $project: {
              name: 1,
              age: 1,
              sex: 1,
              mrNum: 1,
              status:1,
              fk_hosp_id: { $toObjectId: "$beds.fk_hosp_id" },
              bed_Num: "$beds.bed_Num",
            },
          },

          {
            $lookup: {
              from: "Hospital",
              localField: "fk_hosp_id",
              foreignField: "_id",
              as: "hospital",
            },
          },

          { $unwind: "$hospital" },

          {
            $project: {
              name: 1,
              age: 1,
              sex: 1,
              mrNum: 1,
              status:1,
              bed_Num: 1,
              hospital_name: "$hospital.name",
              hospital_code: "$hospital.code",
            },
          },

          /* {
        $match: {
          $or: [
            {
                name: req.name,             
            },
            { 
                hospital_name:req.name 

                
            },
          ],

         
        },
      }, */

          {
            $match: {
              $and:[
              {status:{ $ne: "Discharge" }},
              {
                $or: [
                  { name: { $regex: req.name, $options: "i" } },
                  { hospital_name: { $regex: req.name, $options: "i" } },
                ]
              }
            ]
            },
          },
        ]);



        console.log('commonDAO panameSearch result-------' + JSON.stringify(result))
        if (result) {
            return result;
        } else {
            return null;
        }
    } catch (err) {
        console.log('error in commonDAO panameSearch call----' + err);        
        return null;
    }
};

