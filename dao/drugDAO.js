const drugModel = require("../models/drug");

exports.saveDrug = async (data) => {
  try {
    let drugRes = await new drugModel(data).save();
    if (drugRes != null) {
      return drugRes;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in Save drugRes dut to :" + err);
    return null;
  }
};

exports.getDrugs = async (patient_Id) => {
  try {
    let result = await drugModel.find({ patientId: patient_Id });
    if (result != null) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in getLabs dut to :" + err);
    return null;
  }
};

exports.getactiveDrugs = async (patient_Id) => {
  try {
    let result = await drugModel.find({ patientId: patient_Id, isStop:false});
    if (result != null) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in getLabs dut to :" + err);
    return null;
  }
};

exports.update = async (filter, updateReq) => {
  try {
    let resp = await drugModel.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};


exports.updateDrugEmilsent = async (filter, updateReq) => {
  console.log('updateDrugEmilsent-----',updateReq);
  console.log('updateDrugEmilsent filter-----',filter);
  try {
    let resp = await drugModel.updateMany(filter, updateReq);   
    console.log('updateDrugEmilsent res-----',resp);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};

exports.getDrugsNameDose = async (patient_Id, name, dose,route, frequecy) => {
  try {
    let result = await drugModel.find({ patientId: patient_Id ,drugName :name, dose: dose, route : route, freq: frequecy });
    if (result != null) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in getLabs dut to :" + err);
    return null;
  }
};