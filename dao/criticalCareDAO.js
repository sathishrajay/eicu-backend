const criticalCare = require("../models/criticalCare");

/**
 * @author:Rambabu.K
 * @date:11-10-2020
 * @description:create critical care
 * @param {*} document 
 */
exports.create = async (document) => {

    console.log('criticalCareDAO create request is-----' + JSON.stringify(document));
    try {
        let result = await criticalCare.create(document);
        console.log('criticalCareDAO create result-------' + JSON.stringify(result))
        if (result) {
            return result;
        } else {
            return null;
        }
    } catch (err) {
        console.log('error in criticalCareDAO creation call----' + err);
        logger.error('error in criticalCareDAO creation call----' + err);
        return null;
    }
};

/**
 * @author:Rambabu.K
 * @date:17-10-2020
 * @description:retrieve critical care data
 * @param {*} id 
 */
exports.retrieve = async (id) => {
    console.log('Entered criticalcareDAO Retrieve-----');
    try {
        let result = await criticalCare.findOne({ fk_patient_id:id });
        console.log('criticalcareDAO retrieve result-------' + JSON.stringify(result))
        if (result) {
            return result;
        } else {
            return null;
        }
    } catch (err) {
        console.log('error in criticalcareDAO  fetch is----' + err);
        logger.error('error in criticalcareDAO fetch is----' + err);
        return null;
    }
};



/**
 * @author:Rambabu.K
 * @date:22-10-2020
 * @description:update criticare data
 * @param {*} document 
 */
exports.update = async (filter, updateReq) => {
    console.log('criticalcareDAO update id is-----' + filter);
    console.log('criticalcareDAO update request is-----' + JSON.stringify(updateReq));
    try {
        let resp = await criticalCare.findOneAndUpdate(filter, updateReq);
        if (resp != null) {
            return true;
        } else {
            return false;
        }
    } catch (err) {
        logger.error("Error in criticalcareDAO udate call----- " + err);
        return false;
    }
};