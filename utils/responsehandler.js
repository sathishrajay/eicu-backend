/**
 * this file is for common response like success,fail,error
 * @author:Rambabu
 * Date:17-07-2020
 * @param {*} res
 * @param {*} suc
 * @param {*} err
 */
const responseCreator = function (res, status, suc, err) {
  const responseObj = {
    res,
    status,
    suc,
    err,
  };
  return responseObj;
};

module.exports = {
  responseCreator: responseCreator,
};
