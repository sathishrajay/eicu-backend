/**
 * @description: Common Base Router which will redirect to desired path
 * @author : Rambabu K
 * @date : 20-09-2020
 *
 */

// Express uses path-to-regexp for matching the route paths
const router = require("express").Router();
logger.info("Entered index.js Router -----");

//Controllers
const hosptialController = require("../controllers/hosptial.controller");
const loginController = require("../controllers/login.controller");
const userController = require("../controllers/user.controller");
const patientController = require("../controllers/patient.controller");
const commonController = require("../controllers/common.controller");
const roleController = require("../controllers/role.controller");
const fileuploadController = require("../controllers/fileupload.controller");
const stasisController = require("../controllers/stasis.controller");

//Routers Navigation
router.use("/hospital", hosptialController);

router.use("/user", loginController);
router.use("/usermanagement", userController);
router.use("/patient", patientController);
router.use("/common", commonController);
router.use("/role", roleController);
router.use("/uploadfile", fileuploadController);
router.use("/auth", stasisController);

module.exports = router;
