exports.forgotPasswordEmail = async (emailContent) => {
  try {
    let mailHtml = `   
    <table style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
    <tbody style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
      <tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
        <td class="title" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 20px;line-height: 1.4;color: #000001;">Dear  ${emailContent.name}</td>
      </tr>
      <tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
        <td class="pTB" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;padding-top: 16px;padding-bottom: 16px;">
        We received a request to reset the password on your account.<br/>
        Please use the code to complete your reset
        </td>
      </tr>
    
<tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
<td style="padding-bottom: 16px;font-weight: bold;margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
${emailContent.password}
</td>
</tr>
<tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
<td style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
If this wasn't you , please contact us
</td>
</tr>
<tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
<td style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">Thanks</td>
</tr>
<tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
<td style="padding-bottom: 51px;margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
Team eICU</td>
</tr>
    </tbody>
  </table>
                   
                `;
    return mailHtml;
  } catch (err) {
    console.log("Error occured in forgotUserIdTemplate" + err);
    return "";
  }
};
