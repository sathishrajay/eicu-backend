const moment = require("moment");
exports.opdeprescriptionEmailsTemp = async (emailContent) => {
  try {
    let mailHtml = `
    

    <!doctype html>
 <html>
    <head>
       <title>Template</title>
       <style type="text/css">
          body {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 14px;
          }
          
       </style>
       <meta charset="utf-8">
       <meta content="width=device-width, user-scalable=no" name="viewport">
    </head>
    <body style="background: #f5f5f5;font-family: Arial, Helvetica, sans-serif;font-size: 14px;">
    <h1 style="text-align:center;color: #3F51B5;">ePrescription OPD</h1>   
    <h2 style="text-align:center;">eMed Savers Health Services Pvt. Ltd</h2>
      <table width="980" cellspacing="0" cellpadding="10" style="margin: auto;border: 1px solid #d8d8d8;padding-bottom: 20px;background: #fff;width: 980px;white-space: inherit;word-break: break-word;padding: 0;border-collapse: collapse;">
          
        <tr>
             <td style="border: 1px solid #efefef;padding: 7px 10px;">
                <table width="100%" cellspacing="0" cellpadding="0" style="border: 1px solid #f7f7f7;width: 100%;border-collapse: collapse;">
                   <thead>
                      <tr>
                         <th style="padding: 25px;background: #f7f7f7;text-align: left;border: 1px solid #efefef;font-size: 13px;"><strong>Name</strong></th>
                         <th style="padding: 25px;background: #f7f7f7;text-align: left;border: 1px solid #efefef; font-size: 13px;">Age</th>
                         <th style="padding: 25px;background: #f7f7f7;text-align: left; border: 1px solid #efefef;font-size: 13px;"><strong>Sex</strong></th>
                        
                         
                      </tr>
                   </thead>
                   <tbody>
                      <tr>
                         <td style="padding: 15px;border: 1px solid #efefef;">${
                           emailContent.patient_data.name
                         }</td>
                         <td style="padding: 15px;border: 1px solid #efefef;">${
                           emailContent.patient_data.age
                         }</td>
                         <td style="padding: 15px;border: 1px solid #efefef;">${
                           emailContent.patient_data.sex
                         }</td>                       
                      </tr>
                   </tbody>
                </table>
             </td>
          </tr>

	  <tr>
             <td style="border: 1px solid #efefef;padding: 7px 10px;">
                <table width="100%" cellspacing="0" cellpadding="0" style="border: 1px solid #f7f7f7;width: 100%;border-collapse: collapse;">
                   <thead>
                      <tr>
                         <th style="padding: 25px;background: #f7f7f7;text-align: left;border: 1px solid #efefef;font-size: 13px;"><strong>MR Number</strong></th>
                         <th style="padding: 25px;background: #f7f7f7;text-align: left;border: 1px solid #efefef; font-size: 13px;">Date</th>
                         <th style="padding: 25px;background: #f7f7f7;text-align: left; border: 1px solid #efefef;font-size: 13px;"><strong>Contact Number</strong></th>
                        
                         
                      </tr>
                   </thead>
                   <tbody>
                      <tr>
                         <td style="padding: 15px;border: 1px solid #efefef;">${
                           emailContent.patient_data.mrNum
                         }</td>
                         <td style="padding: 15px;border: 1px solid #efefef;">${new Date().toLocaleDateString()}</td>
                         <td style="padding: 15px;border: 1px solid #efefef;">${
                           emailContent.patient_data.phoneNum
                         }</td>                       
                      </tr>
                   </tbody>
                </table>
             </td>
          </tr>
		  
		   
        
        <tr>
             <td style="border: 1px solid #efefef;padding: 7px 10px;">
                <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                   <tbody>
               <tr><td style="padding: 15px;width: 200px;border: 1px solid #efefef;"><strong>Address:</strong> ${
                 emailContent.patient_data.address
               }</td></tr>
               <tr><td style="padding: 15px;border: 1px solid #efefef;"><strong>Source of History:</strong> ${
                 emailContent.patient_data.sourOfHistory
               }</td></tr>
               <tr><td style="padding: 15px;width: 200px;border: 1px solid #efefef;"><strong>Presenting complaints:</strong> ${
                 emailContent.patient_data.prestComplaints
               }</td></tr>
               <tr><td style="padding: 15px;border: 1px solid #efefef;"><strong>History of Present Illness:</strong> ${
                 emailContent.patient_data.histyOfPrestIllness
               }</td></tr>
               <tr><td style="padding: 15px;border: 1px solid #efefef;"><strong>Medical:</strong> ${
                 emailContent.patient_data.medical
               }</td></tr>
                   </tbody>
                </table>
             </td>
          </tr>
        
        
        <tr>
        <td colspan="4" style="border: 1px solid #efefef;padding: 7px 10px;">
                            <table width="100%" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse; table-layout:fixed">
                               <tr>
                                  <td style="border: 1px solid #efefef;padding: 7px 10px; width:33.3%">
                                     ${
                                       emailContent.patient_data.dm
                                         ? `	<input type="checkbox" checked disabled> DM  
                                     `
                                         : `<input type="checkbox" disabled> DM`
                                     } 
                                  </td>
                          
                                  <td style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.patient_data.ihd
                                      ? `	<input type="checkbox" checked disabled> IHD
                                     `
                                      : `<input type="checkbox" disabled> IHD`
                                  }
                                  </td>
                                  <td style="border: 1px solid #efefef;padding: 7px 10px;"> ${
                                    emailContent.patient_data.cva
                                      ? `	<input type="checkbox" checked disabled> CVA
                                     `
                                      : `<input type="checkbox" disabled> CVA`
                                  } 
                                  </td>
                          </tr>
                          <tr>
                                  <td style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.patient_data.crf
                                      ? `	<input type="checkbox" checked disabled> CRF 
                                     `
                                      : `<input type="checkbox" disabled> CRF`
                                  }
                                  </td>
                                  <td style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.patient_data.htn
                                      ? `	<input type="checkbox" checked disabled> HTN
                                     `
                                      : `<input type="checkbox" disabled> HTN`
                                  }
                                  </td>
                                  <td style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.patient_data.asthma
                                      ? `	<input type="checkbox" checked disabled> Asthma/COPD 
                                     `
                                      : `<input type="checkbox" disabled> Asthma/COPD`
                                  } 
                                  </td>
                          </tr>
                          <tr>
                                  <td colspan="3" style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.patient_data.epilepsy
                                      ? `	<input type="checkbox" checked disabled> Epilepsy
                                     `
                                      : `<input type="checkbox" disabled> Epilepsy`
                                  } 
                                  </td>
                               </tr>
                            </table>
                         </td>
                      </tr>
                 
                 <tr>
                         <td style="padding: 15px;border: 1px solid #efefef;" colspan="2"><strong>Significant History:</strong>   ${
                           emailContent.patient_data.significantHistory
                         }</td>
                      </tr>
                 
        <tr>
        <td colspan="4" style="border: 1px solid #efefef;padding: 7px 10px;">
                            <table width="100%" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse; table-layout:fixed">
                               <tr>
                                  <td style="border: 1px solid #efefef;padding: 7px 10px; width:33.3%">
                                     ${
                                       emailContent.patient_data.smoking
                                         ? `	<input type="checkbox" checked disabled> Smoking
                                     `
                                         : `<input type="checkbox" disabled> Smoking`
                                     } 
                                  </td>
                          
                                  <td style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.patient_data.alcohol
                                      ? `	<input type="checkbox" checked disabled> Alcohol Use
                                     `
                                      : `<input type="checkbox" disabled> Alcohol Use`
                                  }
                                  </td>                                 
                          </tr>								  
                            </table>
                         </td>
                      </tr>
                 
                 
          <tr>
             <td style="border: 1px solid #efefef;padding: 7px 10px;">
                <h2 style="font-size: 20px;margin: 7px 0;padding: 0;color: #3F51B5;text-align: left;">Medication History:</h2>
             </td>
          </tr>
 
          <tr>
             <td style="border: 1px solid #efefef;padding: 7px 10px;">
                <table class="bodyTable" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                   <thead>
                      <tr>
                      <th style="padding: 15px;border: 1px solid #efefef;background: #f7f7f7;text-align: left;font-size: 13px;">Name</th>
                      <th style="padding: 15px;border: 1px solid #efefef;background: #f7f7f7;text-align: left;font-size: 13px;">Dose</th>
                      <th style="padding: 15px;border: 1px solid #efefef;background: #f7f7f7;text-align: left;font-size: 13px;">Route</th>
                      <th style="padding: 15px;border: 1px solid #efefef;background: #f7f7f7;text-align: left;font-size: 13px;">Frequency</th>
                <th style="padding: 15px;border: 1px solid #efefef;background: #f7f7f7;text-align: left;font-size: 13px;">Reconciliation</th>
                         
                      </tr>
               </thead>
               
               
                   <tbody>
                      ${
                        emailContent.patient_data.medicalHistory.length > 0
                          ? emailContent.patient_data.medicalHistory
                              .map(
                                (item, i) => `${
                                  true
                                    ? `
                   <tr>
                   <td class="avenirRmnClr" style="border: 1px solid #efefef;padding: 7px 10px;">${
                     item.name
                   }</td>
                         <td class="avenirRmnClr" style="border: 1px solid #efefef;padding: 7px 10px;">${
                           item.dose
                         }</td>
                         <td class="avenirRmnClr" style="border: 1px solid #efefef;padding: 7px 10px;">${
                           item.route
                         }</td>
                         <td class="avenirRmnClr" style="border: 1px solid #efefef;padding: 7px 10px;">${
                           item.frq
                         }</td>
                   <td class="avenirRmnClr" style="border: 1px solid #efefef;padding: 7px 10px;"> 
                   ${
                     item.isrecncl
                       ? `	<input type="checkbox" checked disabled> Reconciliation 
                      `
                       : `<input type="checkbox" disabled> Reconciliation`
                   } 
                   
                   </td>
                        
                      </tr>						
                   `
                                    : ``
                                }                    
                      `
                              )
                              .join("")
                          : `
                      <tr>
                         <td colspan="5" style="border: 1px solid #efefef;padding: 7px 10px;">No Records to display</td>
                      </tr>
                      `
                      }
                   </tbody>
                </table>
             </td>
          </tr>
                      <tr>
                         <td colspan="4" style="border: 1px solid #efefef;padding: 7px 10px;">
                            <table width="100%" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse; table-layout:fixed">
                               <tr>
                                  <td style="border: 1px solid #efefef;padding: 7px 10px; width:33.3%">
                                     ${
                                       emailContent.patient_data.foodReactions
                                         ? `	<input type="checkbox" checked disabled> Food Reaction : ${emailContent.patient_data.foodDesc}  
                                     `
                                         : `<input type="checkbox" disabled> Food Reaction`
                                     } 
                                  </td>
                          
                                  <td style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.patient_data.drugReactions
                                      ? `	<input type="checkbox" checked disabled> Drug Reaction:${emailContent.patient_data.drugDesc}
                                     `
                                      : `<input type="checkbox" disabled> Drug Reaction`
                                  }
                                  </td>
                                 
                          </tr>                          
                           
                            </table>
                         </td>
                      </tr>                 
          <tr>
             <td style="border: 1px solid #efefef;padding: 7px 10px;">
                <h2 style="font-size: 16px;margin: 7px 0;padding: 0;color: #000;text-align: left;">Vital Data:</h2>
             </td>
          </tr>
          
          <tr>
             <td style="border: 1px solid #efefef;padding: 7px 10px;">
                <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                   <tbody>
                      <tr>
                         <td style="padding: 15px;width: 200px;border: 1px solid #efefef; width:15%"><strong>Pulse/min:</strong> ${
                           emailContent.patient_data.pulse
                         }</td>
                         <td style="padding: 15px;border: 1px solid #efefef; width:15%"><strong>BP mmHg:</strong> ${
                           emailContent.patient_data.bp
                         }</td>
                         <td style="padding: 15px;border: 1px solid #efefef; width:15%"><strong>Resp/min:</strong> ${
                           emailContent.patient_data.resp
                         }</td>
                   <td style="padding: 15px;border: 1px solid #efefef; width:15%"><strong>Temp F:</strong> ${
                     emailContent.patient_data.temp
                   }</td>
                   <td style="padding: 15px;border: 1px solid #efefef; width:15%"><strong>SpO2:</strong> ${
                     emailContent.patient_data.spo
                   }</td>
                   <td style="padding: 15px;border: 1px solid #efefef; width:15%"><strong>WT Kgs:</strong> ${
                     emailContent.patient_data.wt
                   }</td>
                      </tr>
                   </tbody>
                </table>
             </td>
          </tr>
                 
       
       <tr>
             <td style="border: 1px solid #efefef;padding: 7px 10px;">
                <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                   <tbody>
               <tr><td style="padding: 15px;width: 200px;border: 1px solid #efefef;"><strong>Respiratory:</strong> ${
                 emailContent.patient_data.respiratory
               }</td></tr>
               <tr><td style="padding: 15px;border: 1px solid #efefef;"><strong>Abdominal:</strong> ${
                 emailContent.patient_data.abdominal
               }</td></tr>
               <tr><td style="padding: 15px;width: 200px;border: 1px solid #efefef;"><strong>Neurology:</strong> ${
                 emailContent.patient_data.neurology
               }</td></tr>
               <tr><td style="padding: 15px;border: 1px solid #efefef;"><strong>Musculoskeletal:</strong> ${
                 emailContent.patient_data.musculoskeletal
               }</td></tr>
                
                   </tbody>
                </table>
             </td>
          </tr>
        
         <tr>
            <td style="padding: 15px;border: 1px solid #efefef;" colspan="2"><strong>Other Systems:</strong>   ${
              emailContent.patient_data.otherSystems
            }</td>
            </tr>
          
           <tr>
            <td style="padding: 15px;border: 1px solid #efefef;" colspan="2"><strong>Provisional Diagnosis:</strong>   ${
              emailContent.patient_data.provisionalDiagnosis
            }</td>
            </tr>
 
            <tr>
            <td style="padding: 15px;border: 1px solid #efefef;" colspan="2"><strong>Plan of Care:</strong></p>               
            <textarea style="width: 100%;height: 250px; text-align: left;">${
              emailContent.patient_data.planOfCare
                ? emailContent.patient_data.planOfCare
                : ""
            }</textarea>            
            </td>
            </tr>

            <td colspan="3" style="padding: 15px;border: 1px solid #efefef;"><p style="margin:0 0 7px 0"><strong>ePrescription Prepared by : </strong>${
              emailContent.loggedin_user
            }</p>              
                       
            </td>
               </thead>
                </table>
             </td>
          </tr> 
          
        
       </table>
    </body>
 </html>

	`;
    return mailHtml;
  } catch (err) {
    console.log("Error occured in opdeprescriptionEmailsTemp" + err);
    return "";
  }
};
