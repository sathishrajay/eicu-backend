exports.template = async (emailContent) => {
  try {
    let mailHtml =
      `
      <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
      <html lang="en" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
        <head style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
          <meta name="viewport" content="width=device-width, initial-scale=1" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
          <style style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
            * {
              margin: 0 0 0 0;
              font-family: Arial, Verdana;
              font-size: 16px;
              line-height: 1.4;
            }
            p {
              margin: 0;
            }
            a {
              text-decoration: none !important;
              color: #5090f8;
            }
            .title {
              font-size: 20px;
              color: #000001;
            }
            .pTB {
              padding-top: 16px;
              padding-bottom: 16px;
            }
            .pTop-1 {
              padding-top: 16px;
            }
            .pBottom-1 {
              padding-bottom: 16px;
            }
            .accountInfo {
              font-weight: bold;
              color: #8994a5;
            }
          </style>
        </head>
        <body style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
          <table cellspacing="0" width="100%" style="background: #f3f5fd;margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
            <tbody style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
              <tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                <td style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;"></td>
                <td width="675" style="margin: 0 auto 0 auto;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                  <table style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                    <thead style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                      <tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                        <th style="text-align: left;margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                          <img style="padding: 47px 0 32px 0;margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;" src="Eicu" alt="ICU Logo" height="46">
                        </th>
                      </tr>
                    </thead>
                  </table>
                  <table style="padding: 45px 32px 71px 32px;background-color: #ffffff;margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                    <tbody style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                      <tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                        <td style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                        ` +
      emailContent +
      `
                          <table style="background-color: #f3f5fd;padding: 35px 20px 35px 20px;height: auto;margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                            <tbody style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                              <tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                <td width="15%" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                  <img src="http://13.232.98.64/assets/images/lock.png" alt="lock" width="50" height="50" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                </td>
                                <td style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                  <p style="font-size: 18px;font-weight: bold;margin: 0;font-family: Arial, Verdana;line-height: 1.4;">
                                    Check before you click!
                                  </p>
                                 
                                  <p style="color: #8994a5;font-style: italic;font-size: 14px;margin: 0;font-family: Arial, Verdana;line-height: 1.4;">
                                    When you click on a link, the address should
                                    always contain "<span style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                    <a href="www.eicu.com/" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;text-decoration: none !important;color: #5090f8;">eicu.com/".
                                  </a></span></p>
                                  <p style="margin: 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">&nbsp;</p>
                                  <p style="color: #8994a5;font-style: italic;font-size: 14px;margin: 0;font-family: Arial, Verdana;line-height: 1.4;">
                                    Visit the
                                    <span style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                    <a href="" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;text-decoration: none !important;color: #5090f8;">
                                    security.eicu.com/phishing
                                    FAQ site to learn more.
                                  </a></span></p>
                                  <p style="margin: 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">&nbsp;</p>
                                  <p style="color: #8994a5;font-style: italic;font-size: 14px;margin: 0;font-family: Arial, Verdana;line-height: 1.4;">
                                    Don't reply to this email. It was automatically
                                    generated.
                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table style="padding-left: 68px;padding-right: 69px;margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                    <tbody style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                      <tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                        <td style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                          <table style="text-align: center;margin: 0 auto 0 auto;max-width: 550px;padding-top: 32px;padding-bottom: 25px;width: 100%;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                            <tbody style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                              <tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                <td style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;"><a href="#" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;text-decoration: none !important;color: #5090f8;">Legal</a></td>
                                <td style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;"><a href="#" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;text-decoration: none !important;color: #5090f8;">Privacy</a></td>
                                <td style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;"><a href="#" style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;text-decoration: none !important;color: #5090f8;">Security</a></td>
                              </tr>
                            </tbody>
                          </table>

                          <table style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                            <tbody style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                              <tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                <td style="text-align: center;padding-bottom: 25px;color: #8994a5;margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                  &copy; 2020 eICU llc. All rights reserved.
                                </td>
                              </tr>
                              <tr style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                <td style="text-align: center;padding-bottom: 168px;color: #8994a5;margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;">
                                  Terms and conditions, features, support, pricing,
                                  and service options subject to change without
                                  notice.
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td style="margin: 0 0 0 0;font-family: Arial, Verdana;font-size: 16px;line-height: 1.4;"></td>
              </tr>
            </tbody>
          </table>
        </body>
      </html>    
                `;
    return mailHtml;
  } catch (err) {
    console.log("Error occured in header " + err);
    return "";
  }
};
