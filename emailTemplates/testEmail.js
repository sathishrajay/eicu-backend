const moment = require("moment");
exports.testEmailsTemp = async (emailContent) => {
  try {
   let mailHtml = `
   
   <!doctype html>
<html>
   <head>
      <title>Template</title>
      <style type="text/css">
         body {
         font-family: Arial, Helvetica, sans-serif;
         font-size: 14px;
         }
         
      </style>
      <meta charset="utf-8">
      <meta content="width=device-width, user-scalable=no" name="viewport">
   </head>
   <body style="background: #f5f5f5;font-family: Arial, Helvetica, sans-serif;font-size: 14px;">
   <h1 style="text-align:center;color: #3F51B5;">Critical Care Notes</h1>   
   <h2 style="text-align:center;">eMed Savers Health Services Pvt. Ltd</h2>
      <table width="980" cellspacing="0" cellpadding="10" style="margin: auto;border: 1px solid #d8d8d8;padding-bottom: 20px;background: #fff;width: 980px;white-space: inherit;word-break: break-word;padding: 0;border-collapse: collapse;">
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table width="100%" cellspacing="0" cellpadding="0" style="border: 1px solid #f7f7f7;width: 100%;border-collapse: collapse;">
                  <thead>
                     <tr>
                        <th style="padding: 15px;width: 200px;background: #f7f7f7;text-align: left;border: 1px solid #efefef;font-size: 13px;"><strong>Patient Name</strong></th>
                        <th style="padding: 15px;background: #f7f7f7;text-align: left;border: 1px solid #efefef; font-size: 13px;">Age</th>
                        <th style="padding: 15px;background: #f7f7f7;text-align: left;width: 200px;border: 1px solid #efefef;font-size: 13px;"><strong>Sex</strong></th>
                        <th style="padding: 15px;background: #f7f7f7;text-align: left;border: 1px solid #efefef;font-size: 13px;">MR Number</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td style="padding: 15px;border: 1px solid #efefef;">${emailContent.critical_care[0].patientName}</td>
                        <td style="padding: 15px;border: 1px solid #efefef;">${emailContent.critical_care[0].age}</td>
                        <td style="padding: 15px;border: 1px solid #efefef;">${emailContent.critical_care[0].sex}</td>
                        <td style="padding: 15px;border: 1px solid #efefef;">${emailContent.critical_care[0].mrNum}</td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <h2 style="font-size: 20px;margin: 7px 0;padding: 0;color: #3F51B5;text-align: left;">Drug Chart:</h2>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table class="bodyTable" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                  <thead>
                     <tr>
						<th style="padding: 15px;border: 1px solid #efefef;background: #f7f7f7;text-align: left;font-size: 13px;">Drug Name</th>
                        <th style="padding: 15px;border: 1px solid #efefef;background: #f7f7f7;text-align: left;font-size: 13px;">Dose</th>
                        <th style="padding: 15px;border: 1px solid #efefef;background: #f7f7f7;text-align: left;font-size: 13px;">Route</th>
                        <th style="padding: 15px;border: 1px solid #efefef;background: #f7f7f7;text-align: left;font-size: 13px;">Frequency</th>
						<th style="padding: 15px;border: 1px solid #efefef;background: #f7f7f7;text-align: left;font-size: 13px;">Prescribed By</th>
                        
                     </tr>
				  </thead>
				  
				  
                  <tbody>
                     ${
                     emailContent.drug_chart.length > 0
					 ? emailContent.drug_chart.map((item, i) => `${true ?
						`
						<tr>
						<td class="avenirRmnClr" style="border: 1px solid #efefef;padding: 7px 10px;">${item.drugName}</td>
                        <td class="avenirRmnClr" style="border: 1px solid #efefef;padding: 7px 10px;">${item.dose}</td>
                        <td class="avenirRmnClr" style="border: 1px solid #efefef;padding: 7px 10px;">${item.route}</td>
                        <td class="avenirRmnClr" style="border: 1px solid #efefef;padding: 7px 10px;">${item.freq}</td>
						<td class="avenirRmnClr" style="border: 1px solid #efefef;padding: 7px 10px;">${item.prescBy}</td>
                       
                     </tr>						
						`:``					
					}                    
                     `
                     )
                     .join("")
                     : `
                     <tr>
                        <td colspan="5" style="border: 1px solid #efefef;padding: 7px 10px;">No Records to display</td>
                     </tr>
                     `
                     }
                  </tbody>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <h2 style="font-size: 20px;margin: 7px 0;padding: 0;color: #3F51B5;text-align: left;">Notes:</h2>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table width="100%" class="amount-table" cellspacing="4" cellpadding="5" >
                  <tbody>
                     <tr>
                        <td colspan="4" style="color: #000;font-size: 18px;padding: 0; font-size:16px"><strong>Date:</strong> ${
                           new Date(emailContent.critical_care[0].date).toLocaleDateString() 
                           }
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                  <tbody>
				  <tr>
				  <td style="padding: 15px;width: 200px;border: 1px solid #efefef;"><strong>Diagnosis:</strong> ${emailContent.critical_care[0].diagnosis}</td>
				  </tr>
				  <tr><td style="padding: 15px;border: 1px solid #efefef;"><strong>Today's Issues:</strong> ${emailContent.critical_care[0].todaysIssues}</td></tr>
				  <tr><td style="padding: 15px;width: 200px;border: 1px solid #efefef;"><strong>Pending Issues:</strong> ${emailContent.critical_care[0].pendingIssues}</td></tr>
				  <tr><td style="padding: 15px;border: 1px solid #efefef;"><strong>Clinical Trends:</strong> ${emailContent.critical_care[0].clinicalTrends}</td></tr>
                  </tbody>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <h2 style="font-size: 16px;margin: 7px 0;padding: 0;color: #000;text-align: left;">CNS:</h2>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                  <tbody>
                     <tr>
                        <td style="padding: 15px;width: 200px;border: 1px solid #efefef; width:50%"><strong>GCS:</strong> ${emailContent.critical_care[0].gcs}</td>
                        <td style="padding: 15px;border: 1px solid #efefef;"><strong>Pupils:</strong>
						${emailContent.critical_care[0].pupils}
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <h2 style="font-size: 16px;margin: 7px 0;padding: 0;color: #000;text-align: left;">RS:</h2>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                  <tbody>
                     <tr>
                        <td style="padding: 15px;width: 200px;border: 1px solid #efefef;"><strong>Oxygen Theraphy:</strong> ${emailContent.critical_care[0].oxygenTheraphy}</td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <h2 style="font-size: 16px;margin: 7px 0;padding: 0;color: #000;text-align: left;">Mode:</h2>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                  <tbody>
                     <tr>
                        ${
                        emailContent.critical_care[0].invasive
                        ? `
                        <td style="padding: 15px;border: 1px solid #efefef; width:50%">
                           <input type="checkbox" checked disabled> Invasive:  
                        </td>`:`<td style="padding: 15px;border: 1px solid #efefef; width:50%">
                        <input type="checkbox"> Invasive </td>`
                        }
                        ${
                        emailContent.critical_care[0].niv
                        ? `
                        <td style="padding: 15px;border: 1px solid #efefef;">
                           <input type="checkbox" checked disabled> NIV: 
                        </td>`:`<td style="padding: 15px;border: 1px solid #efefef;">
                        <input type="checkbox"> NIV </td>`
                        
                        }
                     </tr>
                     <tr>
                        <td style="padding: 15px;border: 1px solid #efefef;" colspan="2"><strong>Text Area:</strong>   ${emailContent.critical_care[0].textAreaMd}</td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <h2 style="font-size: 16px;margin: 7px 0;padding: 0;color: #000;text-align: left;">CVS:</h2>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                  <tbody>
                     <tr>
                        <td style="padding: 15px;width: 200px;border: 1px solid #efefef; width:25%"><strong>Temp F:</strong> ${emailContent.critical_care[0].temp}</td>
                        <td style="padding: 15px;border: 1px solid #efefef; width:25%"><strong>HR/min:</strong> ${emailContent.critical_care[0].hr}</td>
                        <td style="padding: 15px;border: 1px solid #efefef; width:25%"><strong>BP mmHg:</strong> ${emailContent.critical_care[0].bp}</td>
                        <td style="padding: 15px;border: 1px solid #efefef; width:25%"><strong>Vasopressors:</strong> ${emailContent.critical_care[0].vasopressors}</td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
		 <tr>
		 <tr>
		 <td style="border: 1px solid #efefef;padding: 7px 10px;">
			<h2 style="font-size: 16px;margin: 7px 0;padding: 0;color: #000;text-align: left;">ABG:</h2>
		 </td>
	  </tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;  table-layout: fixed;">
                  <tbody>
                     <tr>
                        <td style="padding: 15px;width: 200px;border: 1px solid #efefef; width:33.3%"><strong>PH:</strong> ${emailContent.critical_care[0].ph}</td>
                        <td style="padding: 15px;border: 1px solid #efefef;"><strong>Pco2 mmHg:</strong> ${emailContent.critical_care[0].pco2}</td>
                        <td style="padding: 15px;border: 1px solid #efefef;"><strong>Po2 mmHg:</strong> ${emailContent.critical_care[0].po2}</td>
                     </tr>
                     <tr>
                        <td style="padding: 15px;border: 1px solid #efefef;"><strong>BE:</strong> ${emailContent.critical_care[0].be}</td>
                        
                        <td style="padding: 15px;border: 1px solid #efefef;"><strong>Hco3:</strong> ${emailContent.critical_care[0].hco3}</td>
                        
                        <td style="padding: 15px;border: 1px solid #efefef;"><strong>Lac:</strong> ${emailContent.critical_care[0].lac}</td>
                       
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <h2 style="font-size: 16px;margin: 7px 0;padding: 0;color: #000;text-align: left;">Renal:</h2>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse; table-layout: fixed">
                  <tbody>
                     <tr>
                        <td style="padding: 15px;width: 200px;border: 1px solid #efefef; width:25%"><strong>Input:</strong> ${emailContent.critical_care[0].inputOutput}</td>
						<td style="padding: 15px;width: 200px;border: 1px solid #efefef; width:25%"><strong>Output:</strong> ${emailContent.critical_care[0].inputOutputTwo}</td>
						
						<td style="padding: 15px;width: 200px;border: 1px solid #efefef; width:25%"><strong>Balance:</strong> ${emailContent.critical_care[0].balance}</td>
                        <td style="padding: 15px;border: 1px solid #efefef; width:25%"><strong>Cum Balance:</strong> ${emailContent.critical_care[0].cumBalance}</td>
                     </tr>
                     <tr>
                        
                     </tr>
                     <tr>
                        ${
                        emailContent.critical_care[0].rrt
                        ? `	
                        <td colspan="4" style="padding: 15px;border: 1px solid #efefef;"><p style="margin:0 0 7px 0"><strong><input type="checkbox" checked disabled>RRT</strong></p>
						${emailContent.critical_care[0].rrtText}
						</td>
                  `:` <td colspan="4" style="padding: 15px;border: 1px solid #efefef;"><input type="checkbox" disabled>RRT</td>`
                        }
                     </tr>
					 <tr>
					 ${
                        emailContent.critical_care[0].motionPassed
                        ? `	
                        <td colspan="4" style="padding: 15px;border: 1px solid #efefef;"><strong><input type="checkbox" checked disabled> Motion Passed</strong></td>
                        `:`<td colspan="4" style="padding: 15px;border: 1px solid #efefef;"><strong><input type="checkbox" disabled> Motion Passed</strong></td>`
                        }  
					 </tr>
					 <tr>
					 <td colspan="4" style="padding: 15px;border: 1px solid #efefef;"><p style="margin:0 0 7px 0"><strong>Perabdomen: </strong></p>
					 ${emailContent.critical_care[0].perabdomen}
					 </td>
                        
					 </tr>
					 <tr>
                        <td colspan="4" style="border: 1px solid #efefef;padding: 7px 10px;">
                           <table width="100%" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse; table-layout:fixed">
                              <tr>
                                 <td style="border: 1px solid #efefef;padding: 7px 10px; width:33.3%">
                                    ${
                                    emailContent.critical_care[0].f
                                    ? `	<input type="checkbox" checked disabled> F:  
                                    `
                                    : `<input type="checkbox" disabled> F: ${emailContent.critical_care[0].fText}`
                                    } 
                                 </td>
                                 <td style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.critical_care[0].a
                                    ? `	<input type="checkbox" checked disabled> A: 
                                    `
                                    : `<input type="checkbox" disabled> A: ${emailContent.critical_care[0].aText}`
                                    }
                                 </td>
                                 <td style="border: 1px solid #efefef;padding: 7px 10px;"> ${
                                    emailContent.critical_care[0].s
                                    ? `	<input type="checkbox" checked disabled> S: 
                                    `
                                    : `<input type="checkbox" disabled> S: ${emailContent.critical_care[0].sText}`
                                    } 
                                 </td>
								 </tr>
								 <tr>
                                 <td style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.critical_care[0].T
                                    ? `	<input type="checkbox" checked disabled> T: 
                                    `
                                    : `<input type="checkbox" disabled> T: ${emailContent.critical_care[0].tText}`
                                    }
                                 </td>
                                 <td style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.critical_care[0].H
                                    ? `	<input type="checkbox" checked disabled> H: 
                                    `
                                    : `<input type="checkbox" disabled> H: ${emailContent.critical_care[0].hText}`
                                    }
                                 </td>
                                 <td style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.critical_care[0].U
                                    ? `	<input type="checkbox" checked disabled> U: 
                                    `
                                    : `<input type="checkbox" disabled> U: ${emailContent.critical_care[0].uText}`
                                    } 
                                 </td>
								 </tr>
								 <tr>
                                 <td colspan="3" style="border: 1px solid #efefef;padding: 7px 10px;">${
                                    emailContent.critical_care[0].G
                                    ? `	<input type="checkbox" checked disabled> G: 
                                    `
                                    : `<input type="checkbox" disabled> G: ${emailContent.critical_care[0].gText}`
                                    } 
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
					 <tr>
					 <td colspan="4" style="padding: 15px;width: 200px;border: 1px solid #efefef;"><p style="margin:0 0 7px 0"><strong>Text Area: </strong></p>
					 ${emailContent.critical_care[0].textArea1}
					 </td>
					 </tr>
					 <tr>
					 <td colspan="4" style="padding: 15px;border: 1px solid #efefef;"><p style="margin:0 0 7px 0"><strong>Pain Score: </strong> </p>${emailContent.critical_care[0].painScore}</td>
					 </tr>
                  </tbody>
               </table>
            </td>
         </tr>

         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <h2 style="font-size: 16px;margin: 7px 0;padding: 0;color: #000;text-align: left;">Infection: </h2>
            </td>
         </tr>
         <tr>
            <td style="border: 1px solid #efefef;padding: 7px 10px;">
               <table width="100%" cellspacing="0" cellpadding="5" style="border: 1px solid #efefef;width: 100%;border-collapse: collapse;">
                  <tbody>
					 <tr>
					 <td style="padding: 15px;border: 1px solid #efefef; width:33.3%"><strong>Culture And Sensitivity: </strong>
					 ${
                           emailContent.critical_care[0].cultureAndAensitivity
                           }
					 </td>
                        <td style="padding: 15px;width: 200px;border: 1px solid #efefef;"><strong>Antibiotics / day: </strong>
						${
                           emailContent.critical_care[0].antibioticsPerday
                           }
						</td>
                        <td style="padding: 15px;width: 200px;border: 1px solid #efefef;"><strong>Invasive Lines: </strong>
						${emailContent.critical_care[0].invasiveLines}
						</td>
                     </tr>
                     <tr>
                        <td colspan="3" style="padding: 15px;border: 1px solid #efefef;"><p style="margin:0 0 7px 0"><strong>Plan for the day:</strong></p>
                     
                      <textarea style="width: 100%;height: 250px; text-align: left;">${emailContent.critical_care[0].planForTheDay}
                      </textarea>
						</td>
                     </tr>                    
                     
                  <td colspan="3" style="padding: 15px;border: 1px solid #efefef;"><p style="margin:0 0 7px 0"><strong>Critical Care Specialist Name:</strong>${emailContent.critical_care[0].logedInUser}</p>                     
                   
                 </td>
                     
                  </tbody>
               </table>
            </td>
         </tr>
		 <tr>
		 </tr>
      </table>
   </body>
</html>
	`;
    return mailHtml;
  } catch (err) {
    console.log("Error�occured�in�forgotUserIdTemplate" + err);
    return "";
  }
};
