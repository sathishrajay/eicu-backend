const router = require("express").Router();
const cron = require("node-cron");
const stasisService = require("../services/stasisServices");
let apiKey=process.env.apiKey;
let apiSecret=process.env.apiSecret;

/**
 *
 * @description: Fetch Authenticate token
 * @author: Rambabu
 * @date :11-01-2021
 *
 */
router.get("/authenticate", function (req, res) {
  console.log("getAuthToken entered auth controller------"); 
  const req_obj={"apiKey":apiKey,"apiSecret":apiSecret}
  console.log('------- obj is---'+JSON.stringify(req_obj));
  stasisService.authenticate(req_obj, function (data) {
    res.send(data);
  });
});

/**
 *
 * @description: Authenticate renew token
 * @author: Rambabu
 * @date :11-01-2021
 *
 */
router.get("/renew", function (request, res) {
  console.log("renew entered auth controller------" + JSON.stringify(request.headers));
  let token=request.headers.authtoken;
  const renew_req_obj={"apiKey":apiKey,"authtoken":token}
  stasisService.renew(renew_req_obj, function (data) {
    res.send(data);
  });
});

/**
 *
 * @description: verify token
 * @author: Rambabu
 * @date :11-01-2021
 *
 */
router.get("/verify", function (req, res) {
  console.log("verify entered auth controller------" + JSON.stringify(req.headers));
  stasisService.verify(req, function (data) {
    res.send(data);
  });
});

/**
 * @description: Fetch Patient Data
 * @author: Rambabu
 * @date :11-01-2021
 */
router.get("/getpatientdata/:id", function (req, res) {
  console.log("retrievecriticalcare entered------" + req.params.id);
  stasisService.getpatientdata(req, function (data) {
    res.send(data);
  });
});


/**
 * @description: Fetch Patient Vital Data
 * @author: Rambabu
 * @date :19-01-2021
 */
router.get("/getpatientvitals/:id/:lastsynctime", function (req, res) {
  console.log("getpatientdvitals entered id------" + req.params.id);
  console.log("getpatientdvitals entered lastSyncTime------" + req.params.lastsynctime);
  stasisService.getpatientvitals(req, function (data) {
    res.send(data);
  });
});

//  0 * * * *(1 hour)
// */1 * * * *(1 min)
//cron.schedule("0 * * * *", () => {  
  //console.log('Schduler in running for every one hour')
 //stasisService.triggerNotifications();
//});

router.get("*", (req, callback) => {
  callback.send("This is authenticate controller");
});

module.exports = router;
