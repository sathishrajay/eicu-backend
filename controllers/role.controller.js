const router = require("express").Router();
const userService = require("../services/userService");

router.post("/saveRole", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  userService.saveRole(req, function (data) {
    res.send(data);
  });
});

router.get("/getRoles", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  userService.retrieveRoles(req, function (data) {
    res.send(data);
  });
});

router.get("/getFunctionalities", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  userService.retrieveFunctionalities(req, function (data) {
    res.send(data);
  });
});

router.get("/getRoleMenu", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  userService.retrieveRoleMenu(req, function (data) {
    res.send(data);
  });
});

router.get("*", (req, callback) => {
  callback.send("This is Role controller");
});

module.exports = router;
