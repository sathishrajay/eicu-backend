const router = require("express").Router();

//Hosptal Services
const commonServices = require("../services/commonServices");


/**
 * @description: Fetch data by using pateint name search
 * @author: Rambabu
 * @date :10-10-2020
 */
router.post("/quicksearch", function (req, res) {
  console.log("Fetch pnamesearch API entered------" + JSON.stringify(req.body));
  logger.info("Fetch pnamesearch API entered-------" + JSON.stringify(req.body));
  commonServices.fetchpanameSearch(req, function (data) {
    res.send(data);
  });
});

router.get("*", (req, callback) => {
  callback.send("This is Hospital controller");
});

module.exports = router;
