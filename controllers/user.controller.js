const router = require("express").Router();
const userService = require("../services/userService");

router.post("/saveUser", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  userService.save(req, function (data) {
    res.send(data);
  });
});

router.post("/getUsers", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  userService.retrieve(req, function (data) {
    res.send(data);
  });
});

router.get("/getUser/:userId", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  userService.retrieveUser(req, function (data) {
    res.send(data);
  });
});

/**
 * @description: Fetch get notifications based on loggedin user id
 * @author: Rambabu
 * @date :03-28-2021
 */
router.get("/getNotifications/:userId", function (req, res) {   
  userService.getNotifications(req, function (data) {
    res.send(data);
  });
});

/**
 * @description: update notifications based on notification_id
 * @author: Rambabu
 * @date :03-28-2021
 */
router.post("/updateNotification", function (req, res) {   
  userService.updateNotification(req, function (data) {
    res.send(data);
  });
});

router.get("*", (req, callback) => {
  callback.send("This is Login controller");
});

module.exports = router;
