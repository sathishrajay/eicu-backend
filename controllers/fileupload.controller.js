const router = require("express").Router();
const { responseCreator } = require("../utils/responsehandler");

const multer = require("multer");
const AWS = require("aws-sdk");
const { v4: uuidv4 } = require('uuid');
const accessKeyId=process.env.accessKeyId
const secretAccessKey=process.env.secretAccessKey
const bucket=process.env.bucket
const s3url=process.env.s3url

// Initializing S3 Interface
const s3 = new AWS.S3({
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey,
});

const storage = multer.memoryStorage({
  destination: function (req, file, callback) {
    callback(null, "");
  },
});

const upload = multer({ storage }).single("image");

router.post("/upload_bkp", upload, (req, res) => {
  console.log("file upload call0000---",req.body);
  const uuid_value=uuidv4();
  console.log('uuid_value in file upload is-----',uuid_value)

  //setting up s3 upload parameters
  const params = {
    Bucket: bucket,
    Key: `${uuid_value}.jpg`, // file name you want to save as
    Body: req.file.buffer,
  };

  s3.upload(params, (error, data) => {
    console.log("file upload call111---");
    if (error) {
      res.send("got error---");
    } else {
      console.log("file upload cal222---");
      res.send("uploaded ----");
    }
  });
  console.log("file upload cal3333---");
  var url=`${s3url}/${uuid_value}.jpg`;
  console.log('Final s3 url is---',url)
  res.send({ url: url });
});


/**
 * @description: File upload API
 * @author: Rambabu
 * @date :21-10-2020 
 */
router.post("/upload", upload, (req, res) => {
  console.log("file upload call in fileupload controller---");
  uploadFile(req,function(data){
    console.log('uploadFile----',data);
    res.send(responseCreator(data, 200, true, null));
  })
});

/**
 * @description: This function will upload the file
 * @author: Rambabu
 * @date :21-10-2020
 */
function uploadFile(req,callback){
  const uuid_value=uuidv4();
  console.log('uuid_value in file upload is-----',uuid_value);
  //File type
  let myfile=req.file.originalname.split(".");
  const filetype=myfile[myfile.length-1];
  //setting up s3 upload parameters
  const params = {
    Bucket: bucket,//s3 bucket name
    Key: `${uuid_value}.${filetype}`, // file name you want to save as
    Body: req.file.buffer,//upload file buffer
  };

  s3.upload(params, (error, data) => {
    console.log("file upload---");
    if (error) {      
      data={ url: '' }
      callback(data);
    } else {
      console.log("upload else part---");     
      var url=`${s3url}/${uuid_value}.${filetype}`;
      console.log('Final s3 url is---',url);
      data={ url: url }
      callback(data);
    }
  });
 
}


/**
 * @description: File upload API
 * @author: Rambabu
 * @date :21-10-2020 
 */
router.post("/delete", upload, (req, res) => {
  console.log("file delete call in fileupload controller---");
  deleteFile(req,function(data){
    console.log('deleteFile----',data);
    res.send(responseCreator(data, 200, true, null));
  })
});

/**
 * @description: This function will upload the file
 * @author: Rambabu
 * @date :21-10-2020
 */
function deleteFile(req,callback){
  const url = req.body.url;
  const fileName = url.substring(url.lastIndexOf('/') + 1);
  
  //setting up s3 upload parameters
  const params = {
    Bucket: "ehealthstorageapp",//s3 bucket name
    Key: fileName, // file name you want to save as
   
  };

  s3.deleteObject(params, (error, data) => {
    console.log("file deleteObject---");
    if (error) {      
      data={ status: "Failed" }
      callback(data);
    } else {
      console.log("deleteObject else part---");   
      
      data={ status: "Sucess" }
      callback(data);
    }
  });
 
}

module.exports = router;