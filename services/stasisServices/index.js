/**
 * @description: This Service class will do Authenticate  operations
 * @author: Rambabu
 * @date :11-01-2021
 */

const JSON = require("circular-json");
const { responseCreator } = require("../../utils/responsehandler");
const { isNotNull, isArrayNotEmpty } = require("../../utils/validators");
const axios = require("axios");
const patientDAO = require("../../dao/patientDAO");
const notificationDAO = require("../../dao/notificationDAO");
const userDAO = require("../../dao/userDAO");
const momentTime = require("moment-timezone");
const moment = require("moment");
const hardcodevitalData = require("./vitalresponse.json");
let apiKey=process.env.apiKey;
let apiSecret=process.env.apiSecret;

var PatientService = {
  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:11-01-2021
   * @description: authenticate
   */
  authenticate: async function (req, callback) {
    console.log("Entered authenticate  service",req);
    const headers = { "Content-Type": "application/json" };
    let AUTH_API=process.env.auth;
    try {
      axios
        .post(`${AUTH_API}`, req, headers)
        .then((res) => {
          if (res.status === 200) {
            callback(responseCreator(res.data.token, "200", true, false));
          } else {
            let fail_obj = { error: "InvalidAuth" };
            callback(responseCreator(fail_obj, "401", true, false));
          }
        }).catch(error => {
          console.log('error in index js authenticate call isss--- ',error);
          let fail_obj = { error: "InvalidAuth" };
          callback(responseCreator(fail_obj, "401", true, false));
      });
    } catch (err) {
      console.log("err occured in authenticate due to : " + err);
      let error_obj = { error: "InvalidAuth" };
      callback(responseCreator(error_obj, "401", true, false));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:11-01-2021
   * @description: renew
   */
  renew: async function (request, callback) {
    console.log("Entered renew  service---");    
    let RENEW_API=process.env.renew;
    let token=request.authtoken;
    let apiKey=request.apiKey;
    const renew_obj={"apiKey":apiKey}
    const req={
      headers : {"Content-Type": "application/json","Authorization":`Bearer ${token}`}
    }

    try {
      axios.post(`${RENEW_API}`, renew_obj, req)
        .then((res) => {
          if (res.status === 200) {
            callback(responseCreator(res.data, "200", true, false));
          } else {
            let fail_bj = { error: "RenewFailed" };
            callback(responseCreator(fail_bj, "401", true, false));
          }
        }).catch(error => {
          console.log('error in index js renew call is----',error);
          let fail_bj = { error: "RenewFailed" };
          callback(responseCreator(fail_bj, "401", true, false));          
      });
    } catch (err) {
      console.log("err occured in renew due to : " + err);
      let error_bj = { error: "RenewFailed" };
      callback(responseCreator(error_bj, "401", true, false));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:11-01-2021
   * @description: verify
   */
  verify: async function (request, callback) {
    console.log("Entered verify  service" + JSON.stringify(request.headers));
    let token=request.headers.authtoken;
    const req={
      headers : {"Content-Type": "application/json","Authorization":`Bearer ${token}`}
    }  
    let verify=process.env.verify;   
    try {
      axios.get(`${verify}`,req).then((res) => {
          if (res.status === 200) {
            callback(responseCreator(res.data, "200", true, false));
          } else {
            let fail_bj = { error: "Unauthorized" };
            callback(responseCreator(fail_bj, "401", true, false));
          }
        }).catch(error => {
          console.log('error in index js verify call is----',error);
          let fail_bj = { error: "Unauthorized" };
          callback(responseCreator(fail_bj, "401", true, false));
        });
    } catch (err) {
      console.log("err occured in verify due to : " + err);
      let error_bj = { error: "Unauthorized" };
      callback(responseCreator(error_bj, "401", true, false));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:11-01-2021
   * @description: Get patientdata
   */
  getpatientdata: async function (request, callback) {
    console.log("Entered getpatientdata  service");
    let token=request.headers.authtoken;
    let getpatient=process.env.getpatient; 
    request.params.id="VMkSIm2f";
    const req={
      headers : {"Content-Type": "application/json","Authorization":`Bearer ${token}`}
    }  

    try {
      axios.get(`${getpatient}${request.params.id}`,req)
        .then((res) => {
          if (res.status === 200) {
            callback(responseCreator(res.data, "200", true, false));
          } else {
            let fail_bj = {
              error: `Patient not found for id ${request.params.id}`,
            };
            callback(responseCreator(fail_bj, "401", true, false));
          }
        }).catch(error => {
          console.log('error in index js getpatientdata is------',error)
          let fail_bj = {error: `Patient not found for id ${request.params.id}`,};
          callback(responseCreator(fail_bj, "401", true, false));
      });
    } catch (err) {
      console.log("err occured in getpatientdata due to : " + err);
      let error_bj = { error: `Patient not found for id ${request.params.id}` };
      callback(responseCreator(error_bj, "401", true, false));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:19-01-2021
   * @description: Get patientvitals
   */
  getpatientvitals: async function (request, callback) {
    console.log("Entered getpatientvitals  service");
    let token=request.headers.authtoken;
    let getpatientvital=process.env.getpatientvital; 
    request.params.id="VMkSIm2f";    
    const req={
      headers : {"Content-Type": "application/json","Authorization":`Bearer ${token}`}
    }  

    try {
      axios.get(`${getpatientvital}${request.params.id}/${request.params.lastsynctime}`,req)
        .then((res) => {
          if (res.status === 200) {
            callback(responseCreator(res.data, "200", true, false));
          } else {
            let fail_bj = {
              error: `Patient not found for id ${request.params.id}`,
            };
            callback(responseCreator(fail_bj, "404", true, false));
          }
        }).catch(error => {
          console.log('error in index js getpatientvitaldata is------'+error.response.status)
          if(error.response.status===404){
            let fail_bj = {error: `Patient not found for id ${request.params.id}`,};
            callback(responseCreator(fail_bj, "404", true, false));
          }else if(error.response.status===401){
            console.log('error in index js getpatientvitaldata call is----');
            let fail_bj = { error: "Unauthorized" };
            callback(responseCreator(fail_bj, "401", true, false));

          }
         
      });
    } catch (err) {
      console.log("err occured in getpatientvitaldata due to : " + err);
      let error_bj = { error: `Patient not found for id ${request.params.id}` };
      callback(responseCreator(error_bj, "404", true, false));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:28-03-2021
   * @description: triggerNotifications Based on time it will come from stasis controller
   */
  triggerNotifications: async function (request, callback) {
    console.log("Entered triggerNotifications  service---");
    try {
      //get all not equal discharge patient Details 
      let patientalldat = await patientDAO.getPatientDetls();

      //get logged is user data based on logged in username
      let userData = await userDAO.validateUser("eicuAdmin");

      //constructing req object for authentication token
      const req_obj = { apiKey: apiKey, apiSecret: apiSecret };
      console.log("------- auth req onject is---" + JSON.stringify(req_obj));

      //get authentication data
      let auth_token = await getautData(req_obj);
      console.log("auth_token is----" + JSON.stringify(auth_token));

      //get localtime zone
      var localTime = momentTime.tz(new Date(), "Asia/Kolkata").format();
      //converting string format
      let date_str = moment(localTime).format("YYYY-MM-DD");

      //Iterating all patient Data
      for (let pat_data of patientalldat) {
        //constructing vital Req
        let vital_req = {
          token: auth_token,//auth_token
          mr_num: patientalldat.mrNum,//Mr_number
          lastsynctime: date_str,//Date format
        };
        //get patient vital data based on mr_number
        //let vital_response = await getPatientVitalData(vital_req); //need to enable once service is up
        let vital_response=hardcodevitalData.res;
        console.log("Vital Response is----" + JSON.stringify(vital_response)); 

        //validating patient vital conditins and applying formulas
        let vital_cond = await checkVitalResponseConditions(vital_response);
        console.log("Vital condtion array is----" + JSON.stringify(vital_cond));

        //Iterating all vital conditions
        for (let vi_cond of vital_cond) {
          //If vital condition true
          if (vi_cond.alert) {
            //Constructing notifications and save into mongo db
            let noti_res = await constructingNotifications(vi_cond, pat_data,userData);
            console.log("noti_res is-----"+JSON.stringify(noti_res));
          }
        }

        break;//need to remove later
      }
    } catch (err) {
      console.log("err occured in triggerNotifications due to : " , err);      
    }
  },
}; 

/**
 * @param {*} req
 * @description:get autenticate data
 * @author:Rambabu
 * @date:03-28-2021 
 */
async function getautData(req) {
  console.log("Entered getautData function service", req);
  const headers = { "Content-Type": "application/json" };
  let AUTH_API = process.env.auth;
  let auth_res=null;
  try {
    await axios
      .post(`${AUTH_API}`, req, headers)
      .then((res) => {
        if (res.status === 200) {
          auth_res=res.data.token;
        } 
      })
      .catch((error) => {
        console.log("error in index js getautData function call isss--- ", error);       
      });
  } catch (err) {
    console.log("err occured in authenticate function due to : " + err);   
  }
  return auth_res;
}

/**
 * @param {*} request
 * @description:getPatientVitalData based on mr_number
 * @author:Rambabu
 * @date:03-28-2021 
 */
async function getPatientVitalData(request) {
  console.log("Entered getPatientVitalData function  service", request);  
    let getpatientvital=process.env.getpatientvital; 
    //request.params.id="VMkSIm2f";   
    //request.params.id=request.patient_id;   
    request.mr_num="VMkSIm2f"; 
    let vital_res=null;
    const req={
      headers : {"Content-Type": "application/json","Authorization":`Bearer ${request.token}`}
    }  
  try {
    await axios.get(`${getpatientvital}${request.mr_num}/${request.lastsynctime}`,req)
      .then((res) => {
        if (res.status === 200) {
          vital_res=res.data;
        } 
      })
      .catch((error) => {
        console.log("error in index js getPatientVitalData function call isss--- ", error);       
      });
  } catch (err) {
    console.log("err occured in getPatientVitalData function due to : " + err);   
    return vital_res;
  }
  return vital_res; 
}

/**
 * @description:Validating Vital Response Conditions
 * @author:Rambabu
 * @date:28-03-2021
 * @param {*} vitalResponse 
 */
function checkVitalResponseConditions(vitalResponse) {
  console.log(
    "Eneterd checkVitalResponseConditions-----" + JSON.stringify(vitalResponse)
  );
  let condtion_arr = [];
  let condition_obj = {};
  try {
    if (isArrayNotEmpty(vitalResponse)) {
      let vitaldata = vitalResponse[0];

      //Red Alarams
      //case:1 Checking SI Condition
      let si_value = vitaldata.ecg_heart_rate / vitaldata.bp_systolic;
      console.log('si_value is----',si_value);
      if (si_value < 0.5 || si_value > 0.7) {
        condition_obj = { label: "SI", value: si_value, alert: true };
        condtion_arr.push(condition_obj);
      }

      //case:2 Calculating MAP value
      let map_value = vitaldata.bp_diastolic - (1 * vitaldata.bp_systolic) / 3;
      console.log('map_value is----',map_value);

      //case:3 Checking MSI Condition
      let msi_value = vitaldata.ecg_heart_rate / map_value;
      console.log('msi_value is----',msi_value);
      if (msi_value < 0.7 || msi_value > 1.3) {
        condition_obj = { label: "MSI", value: msi_value, alert: true };
        condtion_arr.push(condition_obj);
      }
    }
  } catch (err) {
    console.log("Eneterd checkVitalResponseConditions-----", err);
    return condtion_arr;
  }
  return condtion_arr;
}

/**
 * @description:After checking Vital Data conditions,saving the notifications
 * @author:Rambabu
 * @date:28-03-2021
 * @param {*} vitalResCondition 
 */
function constructingNotifications(vitalResCondition,patient_data,userData){
  console.log('Eneterd constructingNotifications vitalResCondition-----'+JSON.stringify(vitalResCondition));
  console.log('Eneterd constructingNotifications patient_data-----'+JSON.stringify(patient_data));
  let res=null;
  try{   
    let notificationReq = {
    notify_ref: patient_data.mrNum,
    ptient_name:patient_data.name,
    notify_type: "Patient Alert",
    priority: "1",
    message: [{
        title: "Notification",
        description: patient_data.name+' and '+vitalResCondition.label+' - '+vitalResCondition.value
      }],
    fk_user_id: userData._id.toString(),
    isActive: true,
    notify_landing: patient_data.hospital_code+"/"+patient_data.bed_Num,
    notify_ref_type: "",
    readStatus: false,
    createdDate: new Date(),
  };
  console.log('Save Notification Request is----'+JSON.stringify(notificationReq))
  res=notificationDAO.saveNotification(notificationReq);
}catch(err){
  console.log('Error in constructingNotifications is----',err)
}
return res;
}

module.exports = PatientService;
