/**
 * @description: This Service class will do Hospital  CriticalCare operations
 * @author: Rambabu
 * @date :11-10-2020
 */

const JSON = require("circular-json");
const { isNotNull, isArrayNotEmpty } = require("../../utils/validators");
const { responseCreator } = require("../../utils/responsehandler");
const criticalCareDAO = require("../../dao/criticalCareDAO");
var criticalCare = require("../../models/criticalCare");
const drugDAO = require("../../dao/drugDAO");
var drug = require("../../models/drug");
const historyDAO = require("../../dao/historyDAO");
const pdf = require("html-pdf");
const emailService = require("../email");
const testEmail = require("../../emailTemplates/testEmail");
//const template = require("../../emailTemplates/template");
const template = require("../../emailTemplates/templatepdf");

var CriticalcareService = {
  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:11-10-2020
   * @description: CriticalCare Creation
   */
  create: async function (req, callback) {
    console.log(
      "Entered CriticalCare create  service" + JSON.stringify(req.body)
    );
    try {
      //if in critical care id is not avaliable below code will execute
      if (!isNotNull(req.body.ccPid)) {
        //if critical care id not avaliable need to insert data
        let cc_res = await criticalCareDAO.create(req.body);

        //get drug details based on patient id
        let drug_result = await drugDAO.getDrugs(req.body.fk_patient_id);

       //get the active drugs
       let active_drugs =  drug_result.filter((item) => !item.isStop);
       console.log('All active_drugs-----'+JSON.stringify(active_drugs));
       let stat_drugs =  drug_result.filter((item) => item.stat);
       console.log('All stat_drugs is-----'+JSON.stringify(stat_drugs));
       //get the stat value true and email sent false records
       let stat_email_drugs =  stat_drugs.filter((item) => item.stat && !item.isEmailSent);
        //get the stat value false and email sent false records
       let stat_email_drugs1 =  active_drugs.filter((item) => !item.stat && !item.isEmailSent);
       console.log('email not sent stat_drugs -----'+JSON.stringify(stat_email_drugs));
       console.log('start false and email false start_drugs1-----'+JSON.stringify(stat_email_drugs1));
      
       //adding two arrays and ignore stat value is true ane emailsent true
       let final_drug_data=[];
       if(isArrayNotEmpty(stat_email_drugs) && isArrayNotEmpty(stat_email_drugs1)){
        final_drug_data=stat_email_drugs.concat(stat_email_drugs1);
       }else{
         if(isArrayNotEmpty(stat_email_drugs)){
          final_drug_data=stat_email_drugs;
         }else if(isArrayNotEmpty(stat_email_drugs1)){
          final_drug_data=stat_email_drugs1;
         }
       }      
       console.log('final_drug_data-----'+JSON.stringify(stat_email_drugs.concat(stat_email_drugs1)));

        

        //constructing criticalcare and drug json data for history saving
        let cc_care = [];
        let drug_ch = [];
        cc_care.push(req.body);
        drug_ch.push(drug_result);

        const obj = {
          patient_id: req.body.fk_patient_id,
          created_date: new Date(),
          created_by: req.body.createdBy,
          critical_care: cc_care,
          drug_chart: final_drug_data,
          hospi_email:req.body.email
        };
        //save drug json and critical care result in istory collection
        let history_res = await historyDAO.create(obj);

        //sending email functionlaity
        sendEmail(obj);
       //After sending emial we have to update the isEmailSent for drug 
        const updateReq={isEmailSent:true}
        const filter={patientId:obj.patient_id,stat:true}
        let drug_isemail_result = await drugDAO.updateDrugEmilsent(filter,updateReq);  
        console.log('drug_isemail_result-----'+JSON.stringify(drug_isemail_result)); 

        console.log(
          "result in create criticalcare api" + JSON.stringify(cc_res)
        );

        if (isNotNull(cc_res)) {
          callback(responseCreator("Saved Successfully...", 200, true, null));
        } else {
          callback(
            responseCreator("Not saved successfully...", 500, false, null)
          );
        }
      } else if (isNotNull(req.body.ccPid)) {
        /* here critical care id is not null then we have to update data into critical care 
      and create new record in history collection */
        //if critical care id avaliable need to update data

        console.log("======= ID ==== :" + req.body.ccPid);
        let cc_res = await criticalCareDAO.update(req.body.ccPid, req.body);

        //get drug details based on patient id
        let drug_result = await drugDAO.getDrugs(req.body.fk_patient_id);
        console.log("test 2 : " + JSON.stringify(req.body.fk_patient_id));
        console.log("test 3 : " + JSON.stringify(drug_result));
       
        //get the active drugs
       let active_drugs =  drug_result.filter((item) => !item.isStop);
       console.log('All active_drugs is-----'+JSON.stringify(active_drugs));
       let stat_drugs =  drug_result.filter((item) => item.stat);
       console.log('All stat_drugs is-----'+JSON.stringify(stat_drugs));
       //get the stat value true and email sent false records
       let stat_email_drugs =  stat_drugs.filter((item) => item.stat && !item.isEmailSent);
        //get the stat value false and email sent false records
       let stat_email_drugs1 =  active_drugs.filter((item) => !item.stat && !item.isEmailSent);
       console.log('email not sent stat_drugs -----'+JSON.stringify(stat_email_drugs));
       console.log('start false and email false start_drugs1-----'+JSON.stringify(stat_email_drugs1));
      
       //adding two arrays and ignore stat value is true ane emailsent true
       let final_drug_data=[];
       if(isArrayNotEmpty(stat_email_drugs) && isArrayNotEmpty(stat_email_drugs1)){
        final_drug_data=stat_email_drugs.concat(stat_email_drugs1);
       }else{
         if(isArrayNotEmpty(stat_email_drugs)){
          final_drug_data=stat_email_drugs;
         }else if(isArrayNotEmpty(stat_email_drugs1)){
          final_drug_data=stat_email_drugs1;
         }
       }
      

       console.log('final_drug_data-----'+JSON.stringify(stat_email_drugs.concat(stat_email_drugs1)));


        //constructing criticalcare and drug json data for history
        let cc_care = [];
        let drug_ch = [];
        cc_care.push(req.body);
        drug_ch.push(drug_result);

        const obj = {
          patient_id: req.body.fk_patient_id,
          created_date: new Date(),
          created_by: req.body.createdBy,
          critical_care: cc_care,
          drug_chart: final_drug_data,
          hospi_email:req.body.email
        };
        console.log("save object " + JSON.stringify(obj));
        //save drug json and critical care result in history collection
        let history_res = await historyDAO.create(obj);
        //sending email functionlaity
        sendEmail(obj);
        //After sending emial we have to update the isEmailSent for drug 
        const updateReq={isEmailSent:true}
        const filter={patientId:obj.patient_id,stat:true}
        let drug_isemail_result = await drugDAO.updateDrugEmilsent(filter,updateReq);  
        console.log('drug_isemail_result-----'+JSON.stringify(drug_isemail_result));  

        console.log(
          "result in criticalcare create api" + JSON.stringify(cc_res)
        );

        if (isNotNull(cc_res)) {
          callback(responseCreator("Saved Successfully...", 200, true, null));
        } else {
          callback(
            responseCreator("Not saved successfully...", 500, false, null)
          );
        }
      }
    } catch (err) {
      console.log(
        "err occured in criticalcare create index js due to : " + err
      );
      logger.error(
        "err occured in criticalcare create index js due to : " + err
      );
      callback(responseCreator(null, 500, false, err));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:17-10-2020
   * @description: CriticalCare Retrieve
   */
  retrieve: async function (req, callback) {
    console.log("Entered CriticalCare retrieve  service" + req.params.id);
    try {
      //below code fetch critical care data based on patient id
      let result = await criticalCareDAO.retrieve(req.params.id);

      //below code fetch cc hisory based on patient id
      let cc_hisitory_result = await historyDAO.retrieve(req.params.id);

      //collecting all  cumbaldate values
      let arr_data=[];
      let cumbalDate=null;
      for(let c in cc_hisitory_result){
        let cc_hist=cc_hisitory_result[c];
        for(let cc in cc_hist.critical_care){
          let data=cc_hist.critical_care[cc];
          if(isNotNull(data.cumBalDate)){
          arr_data.push(new Date(data.cumBalDate));
          }
        }
      }
      console.log('final cumBalDate array is-------------'+JSON.stringify(arr_data));
      var maxDate=new Date(Math.max.apply(null,arr_data));
      console.log('maxDate is-------',new Date(maxDate));     
      var hours = Math.abs(new Date().getTime() - new Date(maxDate).getTime()) / 36e5;
      console.log('hours-------',hours);
      if(hours<24){
        cumbalDate=maxDate;
      }

      console.log('cumbalDate----',cumbalDate);
      //adding cumbal date into cc results
      result.cumBalDate=cumbalDate;

      const obj = {
        cc_results: result,
        cc_history: cc_hisitory_result,
      };

      console.log("result in CriticalCare retrieve api-----" + JSON.stringify(obj));

      if (isNotNull(obj)) {
        callback(responseCreator(obj, 200, true, null));
      } else {
        callback(responseCreator("Data not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in CriticalCare retrieve index js due to : " + err);
      logger.error("err occured in CriticalCare retrieve index js due to : " + err);
      callback(responseCreator(null, 500, false, err));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:22-10-2020
   * @description: retrieveSpecificCCHistory  based on cchistory primary key
   */
  retrieveSpecificCCHistory: async function (req, callback) {
    console.log("Entered retrieveSpecificCCHistory service" + req.params.id);
    try {
      //below code fetch critical care history data based on cc history primary key
      let result = await historyDAO.retrievespecficCChistory(req.params.id);

      console.log(
        "result in retrieveSpecificCCHistory api-----" + JSON.stringify(result)
      );
      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log(
        "err occured in retrieveSpecificCCHistory index js due to : " + err
      );
      logger.error(
        "err occured in retrieveSpecificCCHistory index js due to : " + err
      );
      callback(responseCreator(null, 500, false, err));
    }
  },
};

module.exports = CriticalcareService;

//Sending email
async function sendEmail(mailjson){ 
    let final_result = null;
    console.log("Entered Send email function...." + JSON.stringify(mailjson));
    try {
      const html = await testEmail.testEmailsTemp(mailjson);
      const templates = await template.template(html);  
      var options = {
        format: "A2",
        orientation: "landscape",
      }    
      pdf.create(html, options).toBuffer((err, buffer) => {
        if (err) {
          return console.log("error");
        }
        let email=isNotNull(mailjson.hospi_email)?mailjson.hospi_email:"sjakkina@gmail.com";
        emailService.sendMail(email,"Criticare & Drug Details","Please find your requested attachment",
            [
              {
                filename: "Criticare & Drug.pdf",
                content: buffer,
                contentType: 'application/pdf',
                contentDisposition:'attachment'
              },
            ]
          )
          .then((sendEmailRes) => {
            if (sendEmailRes.status === "success") {
              final_result = responseCreator("", 200, true, null);
              //callback(final_result);
              return final_result;
            } else {
              final_result = responseCreator("", 200, true, null);
              //callback(final_result);
              return final_result;
            }
          });
      });
      let attachemen = [
        {
          filename: "filename.pdf",
          content: Buffer.from(templates, "utf-8"),
          contentType: "blob",
        },
      ];
      final_result = responseCreator("", 200, true, null);
      //callback(final_result);
      return final_result;
    } catch (err) {
      console.log("err occured in send email function due to : " + err);
      final_result = responseCreator(null,200,false,"Unable to get Drug data");
      //callback(final_result);
      return final_result;
    }
  
}
