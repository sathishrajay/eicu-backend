/**
 * @description: This Service class will do Hospital  CRUD operations
 * @author: Rambabu
 * @date :20-09-2020
 */

const JSON = require("circular-json");
const { isNotNull, isArrayNotEmpty } = require("../../utils/validators");
const { responseCreator } = require("../../utils/responsehandler");
const userDAO = require("../../dao/userDAO");
const notificationDAO = require("../../dao/notificationDAO");
const roleDAO = require("../../dao/roleDAO");
const bcrypt = require("bcrypt");
const randomstring = require("randomstring");
const userRegistraion = require("../../emailTemplates/userRegistraion");
const template = require("../../emailTemplates/template");
const emailService = require("../email");

var UserService = {
  /**
   * @package services\branch
   * @param {*} query
   * @param {*} callback
   * @author: Rambabu
   * @date:2020
   * @description: Branch Creation
   */
  save: async function (req, callback) {
    let final_result = null;
    console.log("Entered save create  service" + JSON.stringify(req.body));
    try {
      let user = null;
      let data = req.body;
      data.userName = req.body.userName.trim();
      data.createdBy = "";
      data.createdDate = new Date();
      data.modifiedBy = "";
      data.modifiedDate = new Date();
      console.log("user save : " + JSON.stringify(data));
      if (isNotNull(data._id)) {
        const filter = { _id: data._id.toString() };
        user = await userDAO.update(filter, data);
      } else {
        let token = randomstring.generate(7);
        data.password = bcrypt.hashSync(token, 10);
        delete data._id;
        data.isVerified = false;
        user = await userDAO.save(data);
        if (user) {
          let emailData = {};
          emailData.name = data.firstName + " " + data.lastName;
          emailData.userName = data.userName;
          emailData.password = token;
          const html = await userRegistraion.userRegistraionEmail(emailData);
          const templates = await template.template(html);

          await emailService.sendMail(
            data.email,
            "User Registraion",
            templates
          );
          final_result = responseCreator(user, 200, true, null);
        }
      }

      console.log("user save : " + JSON.stringify(user));
      if (user) {
        final_result = responseCreator(user, 200, true, null);
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to create User"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in saving user due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to create User");
      callback(final_result);
    }
  },
  retrieve: async function (req, callback) {
    console.log("Entered User retrieve api call");
    try {
      let result = await userDAO.findAll();

      console.log("result in User hospital api" + JSON.stringify(result));

      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data is not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in retrieve User index js due to : " + err);
      logger.error("err occured in retrieve Userß index js due to : " + err);
      callback(responseCreator("Data not avaliable...", 500, false, err));
    }
  },
  /**
   * @package services\branch
   * @param {*} query
   * @param {*} callback
   * @author: Rambabu
   * @date:2020
   * @description: Branch Creation
   */
  saveRole: async function (req, callback) {
    let final_result = null;
    console.log("Entered save role  service" + JSON.stringify(req.body));
    try {
      let user = null;
      let data = req.body;
      data.createdBy = data.userId;
      data.createdDate = new Date();
      data.modifiedBy = data.userId;
      data.modifiedDate = new Date();

      if (isNotNull(data._id)) {
        const filter = { _id: data._id.toString() };
        user = await roleDAO.update(filter, data);
      } else {
        delete data._id;
        user = await roleDAO.save(data);
      }

      console.log("role save " + JSON.stringify(user));
      if (user) {
        final_result = responseCreator(user, 200, true, null);
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to create User"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in saving role due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to create Role");
      callback(final_result);
    }
  },
  retrieveRoles: async function (req, callback) {
    console.log("Entered retrieveRoles api call");
    try {
      let result = await roleDAO.findAll();

      console.log("result in retrieveRoles " + JSON.stringify(result));

      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data is not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in retrieveRoles js due to : " + err);
      logger.error("err occured in rretrieveRoles js due to : " + err);
      callback(responseCreator("Data not avaliable...", 500, false, err));
    }
  },
  retrieveFunctionalities: async function (req, callback) {
    console.log("Entered retrieveFunctionalities retrieve api call");
    try {
      const result = {
        permissions: [
          {
            code: "MOD-1",
            name: "Add Hospital",
          },
          {
            code: "MOD-2",
            name: "View Hospital",
          },
          {
            code: "MOD-3",
            name: "Edit Hospital",
          },
          {
            code: "MOD-4",
            name: "Add Patient",
          },
          {
            code: "MOD-5",
            name: "View Patient",
          },
          {
            code: "MOD-6",
            name: "Edit Patient",
          },
          {
            code: "MOD-7",
            name: "Add Lab",
          },
          {
            code: "MOD-8",
            name: "View Lab",
          },
          {
            code: "MOD-9",
            name: "Edit Lab",
          },
          {
            code: "MOD-10",
            name: "Add Drug",
          },
          {
            code: "MOD-11",
            name: "View Drug",
          },
          {
            code: "MOD-12",
            name: "Edit Drug",
          },
          {
            code: "MOD-13",
            name: "Add Critical Care Notes",
          },
          {
            code: "MOD-14",
            name: "View Critical Care Notes",
          },
          {
            code: "MOD-15",
            name: "Add User",
          },
          {
            code: "MOD-16",
            name: "View User",
          },
          {
            code: "MOD-17",
            name: "Edit User",
          },
          {
            code: "MOD-18",
            name: "Add Role",
          },
          {
            code: "MOD-19",
            name: "Edit Role",
          },
          {
            code: "MOD-20",
            name: "View Role",
          },
        ],
      };

      console.log("result in User hospital api" + JSON.stringify(result));

      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data is not avaliable...", 500, true, null));
      }
    } catch (err) {
      console.log("err occured in retrieve User index js due to : " + err);
      logger.error("err occured in retrieve Userß index js due to : " + err);
      callback(responseCreator("Data not avaliable...", 500, false, err));
    }
  },
  retrieveRoleMenu: async function (req, callback) {
    console.log("Entered retrieveRoleMenu api call");
    try {
      let result = await roleDAO.getRoleMenu();

      console.log("result in retrieveRoles " + JSON.stringify(result));

      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data is not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in retrieveRoleMenu js due to : " + err);
      logger.error("err occured in retrieveRoleMenu js due to : " + err);
      callback(responseCreator("Data not avaliable...", 500, false, err));
    }
  },
  retrieveUser: async function (req, callback) {
    console.log("Entered User retrieveUser api call");
    try {
      let result = await userDAO.getUserForId(req.params.userId);

      console.log("result in User retrieveUser api" + JSON.stringify(result));

      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data is not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in retrieveUser User index js due to : " + err);
      logger.error(
        "err occured in retrieveUser Userß index js due to : " + err
      );
      callback(responseCreator("Data not avaliable...", 500, false, err));
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} callback
   * @description:Get notificataions based on logged in userid
   * @author:Rambabu
   * @date:03-28-2021
   */
  getNotifications: async function (req, callback) {
    console.log("Entered User getNotifications api call");
    try {
      let result = await notificationDAO.getNotification(req.params.userId);
      console.log(
        "result in User getNotifications api" + JSON.stringify(result)
      );
      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data is not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log(
        "err occured in getNotifications User index js due to : ",
        err
      );
      callback(responseCreator("Data not avaliable...", 500, false, err));
    }
  },


  /**
   *
   * @param {*} req
   * @param {*} callback
   * @description:Update Notification based on notification id
   * @author:Rambabu
   * @date:03-28-2021
   */
  updateNotification: async function (req, callback) {
    console.log("Entered User updateNotification api call");
    try {    
     
      const filter = { _id: req.body._id.toString() };
      let update_obj={readStatus: true}      
      let result = await notificationDAO.updateNotification(filter,update_obj);
      console.log(
        "result in User updateNotification api" + JSON.stringify(result)
      );
      if (isNotNull(result)) {
        callback(responseCreator('Updated Successfully', 200, true, null));
      } else {
        callback(responseCreator("Notification not updated...", 500, false, null));
      }
    } catch (err) {
      console.log(
        "err occured in updateNotification User index js due to : ",
        err
      );
      callback(responseCreator("Notification not updated...", 500, false, err));
    }
  },


};

module.exports = UserService;
