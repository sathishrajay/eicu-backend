/**
 * @description: This Service class will do Hospital  CRUD operations
 * @author: Rambabu
 * @date :20-09-2020
 */

const JSON = require("circular-json");
const { responseCreator } = require("../../utils/responsehandler");
const patientDAO = require("../../dao/patientDAO");
const bedsDAO = require("../../dao/bedsDAO");
const labDAO = require("../../dao/labDAO");
const bcrypt = require("bcrypt");
const moment = require("moment");
const drugDAO = require("../../dao/drugDAO");
const emailService = require("../email");
const testEmail = require("../../emailTemplates/testEmail");
const template = require("../../emailTemplates/template");
const { isNotNull } = require("../../utils/validators");
const pdf = require("html-pdf");
const userDAO = require("../../dao/userDAO");
const spocDAO = require("../../dao/spocDAO");
const momentTime = require("moment-timezone");
const dischargeEmail = require("../../emailTemplates/dischargeEmail"); 
const opdEprescriptionmail = require("../../emailTemplates/opdEprescriptionmail"); 
const dischargetemplatepdf = require("../../emailTemplates/dischargetemplatepdf");
const hospitalDAO = require("../../dao/hospitalDAO");
const OpdHistory = require("../../models/opdHistoryModel");


var PatientService = {
  /**
   * @package services\branch
   * @param {*} query
   * @param {*} callback
   * @author: Rambabu
   * @date:2020
   * @description: Branch Creation
   */
  save: async function (req, callback) {
    let final_result = null;
    let isReconciledDrugs = null;
    console.log("Entered save create  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let patientId =null;
      if(data.medicalHistory){
        isReconciledDrugs =  data.medicalHistory.filter((item) => item.isrecncl);
      }
      let patient = null;
      let bedsRes = null;
      bedsRes = await bedsDAO.getBedForHospitalId(
        data.hosp_id,
        data.bedNum
      );
      if (bedsRes) {
        //ignore
      } else {
        let bed = {};
        bed.bed_Num = data.bedNum;
        bed.fk_hosp_id = data.hosp_id;
        bed.status = "vacant";
        bed.createdBy = data.usrid;
        bed.createdDate = new Date();
        bed.modifiedBy = data.usrid;
        bed.modifiedDate = new Date();
        bedsRes = await bedsDAO.save(bed);
      }

      if (bedsRes) {
        data.bed_id = bedsRes._id.toString();
        data.modifiedBy = data.usrid;
        data.modifiedDate = new Date();
        
        if (isNotNull(data._id)) {
          patientId = data._id;
          const filter = { _id: data._id };
          patient = await patientDAO.update(filter, data);
        } else {
          delete data._id;
          data.createdBy = data.usrid;
          data.status='';//by default empty for patient discharge status
          data.createdDate = new Date();
          patient = await patientDAO.save(data);
          patientId = patient._id;
          const filter = { _id: bedsRes._id.toString() };
          let bedUpdate = await bedsDAO.update(filter, {
            fk_patient_id: patient._id.toString(),
            status: "occupy",
          });
        }
        if(isReconciledDrugs){
          const userData = await userDAO.getUserForId(data.usrid);
          let preScribedBy = "";
          if(userData){
            preScribedBy = userData.firstName + " "+ userData.lastName;
          }
          for(let drug of isReconciledDrugs){
            let newDrug ={};
           let drugData =  await drugDAO.getDrugsNameDose(patientId,drug.name,drug.dose,drug.route,drug.frq);
           if(drugData.length ===0){
            const newDate = new Date();
            newDrug.drugName = drug.name;
            newDrug.prescBy = preScribedBy;
            newDrug.dateTime = newDate;
            newDrug.dose = drug.dose;
            newDrug.route = drug.route;
            newDrug.freq = drug.frq;
            newDrug.stopDate = newDate;
            newDrug.isStop = false;
            newDrug.modifiedDate = newDate;
            newDrug.modifiedBy = data.usrid;
            newDrug.patientId = patientId;
            newDrug.createdDate = newDate;
            newDrug.createdBy = data.usrid;
            let drugRes = await drugDAO.saveDrug(newDrug);
           }
          }

        }
        console.log("Patient saved : " + JSON.stringify(patient));
        if (patient) {
          final_result = responseCreator(patient, 200, true, null);
        } else {
          final_result = responseCreator(
            null,
            200,
            false,
            "Unable to create patient"
          );
        }
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to create patient"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in saving user due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to create User");
      callback(final_result);
    }
  },
  saveLab: async function (req, callback) {
    let final_result = null;
    console.log("Entered save create  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let responsedata = [];
      for (let lab of data.labdata) {
        
        lab.modifiedDate = new Date();
        lab.modifiedBy = lab.createdBy;
        if (isNotNull(lab._id)) {
          delete lab.date; 
          const filter = { _id: lab._id };
          labRes = await labDAO.update(filter, lab);
          if (labRes) {
            responsedata.push(labRes);
          }
        }else{
          lab.date = new Date();
          lab.createdDate = new Date();
          delete lab._id; 
          let labRes = await labDAO.saveLabs(lab);
        if (labRes) {
          responsedata.push(labRes);
        }

        }

        
      }
      console.log("saveLab == " + JSON.stringify(responsedata));

      if (responsedata) {
        final_result = responseCreator(responsedata, 200, true, null);
      } else {
        final_result = responseCreator(null, 200, false, "Unable to save Labs");
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in saveLab due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to save Labs");
      callback(final_result);
    }
  },
  getLabData: async function (req, callback) {
    let final_result = null;
    console.log("Entered getLabData  service" + JSON.stringify(req.body));
  

    try {
      let data = req.body;
      let responsedata = [];
      let results = await labDAO.getLabs(data.patientId);

      console.log("getLab == " + JSON.stringify(results));

      if (results) {
        final_result = responseCreator(results, 200, true, null);
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to get Lab data"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in getLabData due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Unable to get Lab data"
      );
      callback(final_result);
    }
  },
  saveDrug: async function (req, callback) {
    let final_result = null;
    console.log("Entered saveDrug create  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let responsedata = [];
      for (let drug of data.drugdata) {
        
        drug.modifiedDate = new Date();
        drug.modifiedBy = data.usrid;
        drug.patientId = data.patientId;
        if (isNotNull(drug._id)) {
          delete drug.dateTime; 
          delete drug.stopDate; 
          const filter = { _id: drug._id.toString() };
          let results = await drugDAO.update(filter, drug);
          if (results) {
            responsedata.push(results);
          }
        } else {
          delete drug._id; 
          if(drug.stat){
            drug.isEmailSent =false;
            drug.isStop =true;
          }
          drug.dateTime = new Date();
          drug.stopDate = new Date();
          drug.createdDate = new Date();
          drug.createdBy = data.usrid;
          let drugRes = await drugDAO.saveDrug(drug);
          if (drugRes) {
            responsedata.push(drugRes);
          }
        }
      }
      console.log("drugRes == " + JSON.stringify(responsedata));

      if (responsedata) {
        final_result = responseCreator(responsedata, 200, true, null);
      } else {
        final_result = responseCreator(null, 200, false, "Unable to save Labs");
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in saveDrug due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to save Drug");
      callback(final_result);
    }
  },
  getDrugData: async function (req, callback) {
    let final_result = null;
    console.log("Entered getDrugData  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let responsedata = [];
      let results = await drugDAO.getDrugs(data.patientId);

      console.log("getDrugData == " + JSON.stringify(results));
      for(let drug of results){
        let newDrug = {};
        newDrug._id = drug._id;
        newDrug.drugName = drug.drugName;
        newDrug.prescBy = drug.prescBy;
        var localTime = momentTime
        .tz(drug.dateTime, "Asia/Kolkata")
        .format();
        newDrug.dateTime = moment(localTime).format('YYYY-MM-DD HH:mm');;
        newDrug.isStop = drug.isStop;
        newDrug.dose = drug.dose;
        newDrug.route = drug.route ;
        newDrug.freq = drug.freq;
        newDrug.stat = drug.stat;
        newDrug.infusion = drug.infusion;
        if(drug.stopDate){
          var localStoppedTime = momentTime
        .tz(drug.stopDate, "Asia/Kolkata")
        .format();
        

        newDrug.stopDate = moment(localStoppedTime).format('YYYY-MM-DD HH:mm');
      }
      newDrug.modifiedDate = drug.modifiedDate;
        newDrug.modifiedBy = drug.modifiedBy;
        newDrug.patientId = drug.patientId
        

        newDrug.createdDate = drug.createdDate;
        newDrug.createdBy = drug.createdBy;

        if(drug && drug.stoppedByUserId){
          const userData = await userDAO.getUserForId(drug.stoppedByUserId);
          if(userData){
            newDrug.stoppedBy = userData.firstName + " "+ userData.lastName;
          }

        }
        responsedata.push(newDrug);        

      }

      if (results && results.length > 0) {
        final_result = responseCreator(responsedata, 200, true, null);
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to get Drug data"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in getDrugData due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Unable to get Drug data"
      );
      callback(final_result);
    }
  },
  sendEmailPdf: async function (req, callback) {
    let final_result = null;
    console.log("Entered getDrugData  service" + JSON.stringify(req.body));
    try {
      const html = await testEmail.testEmailsTemp({ test: "nme" });
      const templates = await template.template(html);
      // const PDF = await emailService.createPDF(templates, {});
      pdf.create(templates, {}).toBuffer((err, buffer) => {
        if (err) {
          return console.log("error");
        }
        emailService
          .sendMail(
            req.body.email,
            "Test Eamil Log",
            "Please find your requested attachment",
            [
              {
                filename: "Test.pdf",
                content: buffer,
              },
            ]
          )
          .then((sendEmailRes) => {
            if (sendEmailRes.status === "success") {
              final_result = responseCreator("", 200, true, null);
              callback(final_result);
            } else {
              final_result = responseCreator("", 200, true, null);
              callback(final_result);
            }
          });
      });
      let attachemen = [
        {
          filename: "filename.pdf",
          content: Buffer.from(templates, "utf-8"),
          contentType: "blob",
        },
      ];

      final_result = responseCreator("", 200, true, null);
      callback(final_result);
    } catch (err) {
      console.log("err occured in getDrugData due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Unable to get Drug data"
      );
      callback(final_result);
    }
  },
  stopDrup: async function (req, callback) {
    let final_result = null;
    console.log("Entered stopDrup  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let responsedata = [];
      const filter = { _id: data._id.toString() };
      data.isStop = true;
      data.stopDate = new Date();
      data.modifiedDate = new Date();
      data.modifiedBy = data.usrid;
      data.stoppedByUserId = data.usrid;
      let results = await drugDAO.update(filter, data);

      console.log("getDrugData == " + JSON.stringify(results));

      if (results) {
        final_result = responseCreator(results, 200, true, null);
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to get Drug data"
        );
      }

      
    } catch (err) {
      console.log("err occured in getDrugData due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Unable to get Drug data"
      );
      callback(final_result);
    }
    callback(final_result);
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:07-11-2020
   * @description: Decharge Patient based on hospital_id and bed_num
   */
  dischargePatient:async function(req, callback){    
    console.log("Entered dischargePatient  service" + JSON.stringify(req.body));
    let final_result = "";
    try {      
      const bed_filter = { bed_Num: req.body.bed_Num, fk_hosp_id: req.body.hid };
      const bed_data = { status: "vacant", fk_patient_id: "" };

      //update beds collection based on hospital_id and bed_num
      let results = await bedsDAO.dischargePatient(bed_filter, bed_data);
      console.log("discharge bed result in patientservice == " +JSON.stringify(results));

      //update patient collection based on fk_patient_id      
      const patient_filter = { _id: req.body.patient_id};
      const patient_data = { status: "Discharge",dischargedesc:req.body.dischargedesc,diagnosis:req.body.diagnosis,remarks:req.body.remarks};
      let patient_results = await patientDAO.updatePatientStatus(patient_filter, patient_data);
      console.log("discharge Patient result in patientservice == " +JSON.stringify(patient_results));      
      if (results) {
        final_result = responseCreator("Decharged Successfully!...",200,true,null);
      } else {
        final_result = responseCreator(null,200,false,"Decharged not successfully");
      }
    } catch (err) {
      console.log("err occured in dischargePatient due to : " + err);
      final_result = responseCreator(null,200,false,"Decharged not successfully");
      callback(final_result);
    }
    //Constructing discharge summary for email
    /*  PatientService.constructingdischargesummaryformail(req, function (data) {
      callback(final_result);
    });  */
    PatientService.constructingdischargesummaryformail(req);
    callback(final_result);
  },

  constructingdischargesummaryformail: async (req) => {
    //Fetch Patient details
    let patint_result = await patientDAO.getpatientDetails(req.body.patient_id);
    console.log('patint_result-----'+JSON.stringify(patint_result));

    //Fetch active drugs
    let activedrug_results = await drugDAO.getactiveDrugs(req.body.patient_id);
    console.log('activedrug_results-----'+JSON.stringify(activedrug_results));

    //Fetch hospital email
    let hospital_email = await hospitalDAO.fetchHospitalEmail(req.body.hid);  
    console.log('hospital_email-----'+JSON.stringify(hospital_email)); 

    //Fetch Logged in username based on mongodb is
    let loggedin_userdata = await userDAO.getUserForId(req.body.loggedin_user);  
    console.log('loggedin_userdata-----'+JSON.stringify(loggedin_userdata)); 
    let loggedin_username=isNotNull(loggedin_userdata)?loggedin_userdata.firstName+' '+loggedin_userdata.lastName:'';

    //construing mail json obj
    let mail_json = {
      patient_data: patint_result,
      drug_chart: activedrug_results,
      hospital_email:hospital_email,
      loggedin_user:loggedin_username
    };

  //Sending email
  PatientService.sendEmail(mail_json);
  },
  sendEmail: async (mailjson) => {
    let final_result = null;
    console.log("Entered Send email function...." + JSON.stringify(mailjson));
    try {
      let html = await dischargeEmail.dischargeEmailsTemp(mailjson);    
      //const templates = await dischargetemplatepdf.dischargetemplate(html);
      var options = {
        format: "A2",
        orientation: "landscape",
      };
      pdf.create(html, options).toBuffer((err, buffer) => {
        if (err) {
          return console.log("error");
        }
       let mail_id=mailjson.hospital_email!=null?mailjson.hospital_email:"sjakkina@gmail.com"
        emailService
          .sendMail(
            mail_id,
            "Discharge Summary",
            "Please find your requested attachment",
            [
              {
                filename: "DischargeSummary.pdf",
                content: buffer,
                contentType: "application/pdf",
                contentDisposition: "attachment",
              },
            ]
          )
          .then((sendEmailRes) => {
            if (sendEmailRes.status === "success") {
              final_result = responseCreator("", 200, true, null);
              //callback(final_result);
              return final_result;
            } else {
              final_result = responseCreator("", 200, true, null);
              //callback(final_result);
              return final_result;
            }
          });
      });
    /*   let attachemen = [
        {
          filename: "filename.pdf",
          content: Buffer.from(templates, "utf-8"),
          contentType: "blob",
        },
      ]; */
      final_result = responseCreator("", 200, true, null);
      //callback(final_result);
      return final_result;
    } catch (err) {
      console.log("err occured in send email function due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Unable to get Drug data"
      );
      //callback(final_result);
      return final_result;
    }
  },


  
/**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:12-07-2020
   * @description: Get trending data
   */
  gettrendingdata: async function (req, callback) {
    let final_result = null;
    console.log("Entered gettrendingdata  service" + JSON.stringify(req.body));  

    try {
      let data = req.body;     
      let results = await labDAO.getTrendingData(data);
      console.log("gettrendingdata is == " + JSON.stringify(results));

      if (results) {
        final_result = responseCreator(results, 200, true, null);
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to get trending data"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in gettrendingdata due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Unable to get trending data"
      );
      callback(final_result);
    }
  },
  saveSpoc: async function (req, callback) {
    let final_result = null;
    console.log("Entered saveSpoc create  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      data.createdDate = new Date();
      data.createdBy = data.usrid;
      let responsedata = await spocDAO.saveSpoc(data);
      
      console.log("saveSpoc == " + JSON.stringify(responsedata));

      if (responsedata) {
        final_result = responseCreator(responsedata, 200, true, null);
      } else {
        final_result = responseCreator(null, 200, false, "Unable to save Spoc");
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in saveSpoc due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to save saveSpoc");
      callback(final_result);
    }
  },
  getSpocData: async function (req, callback) {
    let final_result = null;
    console.log("Entered getSpocData create  service" + JSON.stringify(req.body));
    try {
      let responsedata = null;
      let userData = null;
      if(req.params.id){
     responsedata = await spocDAO.getSpoc(req.params.id);
      console.log("getSpocData == " + JSON.stringify(responsedata));
    }
      if (responsedata && responsedata.length >0) {
        let userIds = [];
        responsedata.forEach((spocRes) => {
          userIds.push(spocRes.createdBy);
        });
        if(userIds.length >0){
          userData = await userDAO.getUserDataForIds(userIds);
        }

        for(let spoc of responsedata){
          const usr = userData.filter(
            (userInfo) =>
            userInfo.id.toString() === spoc.createdBy
          );
          if (usr && usr.length > 0) {
            spoc.createdBy = usr[0].firstName + " " + usr[0].lastName;
          }
          var localTime = momentTime
        .tz(spoc.createdDate, "Asia/Kolkata")
        .format();
        spoc.createdDate = moment(localTime).format('YYYY-MM-DD HH:mm');;

        }

        final_result = responseCreator(responsedata, 200, true, null);
      } else {
        final_result = responseCreator(null, 200, false, "Unable to  get SpocData");
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in getSpocData due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to getSpocData");
      callback(final_result);
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:02-05-2021
   * @description: sendOPDEprescriptionmail to hospital mail id
   */
  sendOPDEprescriptionmail: async function (req, callback) {
    console.log(
      "Entered sendOPDEprescriptionmail  service" + JSON.stringify(req.body)
    );
    let final_result = "";
    let respDb = null;
    try {
      //Fetch Patient details
      let patint_result = await patientDAO.getpatientDetails(req.body.patient_id);
      console.log("constructingOPDEprescriptionmail patint_result-----" +JSON.stringify(patint_result));
      
      // persisting opd history record
      if(patint_result){
         let patient = patint_result;
         if(patient._id){
          let patId = patient._id.toString();
          delete patient._id;
          patient.patient_id = patId;
          patient.createdDate = new Date();
          patient.modifiedDate = new Date();
          respDb = await patientDAO.saveOpdHistory(patient);
         }
      }
      //Fetch hospital email
      let hospital_email = await hospitalDAO.fetchHospitalEmail(req.body.hid);
      console.log("constructingOPDEprescriptionmail hospital_email-----" +JSON.stringify(hospital_email));

      //Fetch Logged in username based on mongodb is
      let loggedin_userdata = await userDAO.getUserForId(
        req.body.loggedin_user
      );
      console.log(
        "constructingOPDEprescriptionmail loggedin_userdata-----" +
          JSON.stringify(loggedin_userdata)
      );
      let loggedin_username = isNotNull(loggedin_userdata)
        ? loggedin_userdata.firstName + " " + loggedin_userdata.lastName
        : "";

      //construing mail json obj
      let mail_json = {
        patient_data: patint_result,
        hospital_email: hospital_email,
        loggedin_user: loggedin_username,
      };
      let hisDate = null;
      if(respDb){
        var localTime = momentTime
      .tz(respDb.createdDate, "Asia/Kolkata")
      .format();
      hisDate = moment(localTime).format('YYYY-MM-DD HH:mm');

      }

      //Sending email
      final_result = await PatientService.OPDEprescriptionmail(mail_json);
      final_result.historyId = respDb ? respDb._id.toString():"";
      final_result.historyCreationDt = hisDate ? hisDate:"";
      
      callback(final_result);
         
    } catch (err) {
      console.log("err occured in sendOPDEprescriptionmail due to : " + err);
      final_result = responseCreator(null, 200, false, "Mail not successfully");
      callback(final_result);
    }
  },   

  /**
   * @author: Varma
   * @date:27-05-2021
   * @description: retrieveOpdHistory
   */
  retrieveOpdHistory: async function (req, callback) {
    console.log("Entered retrieveOpdHistory service" + req.params.id);
    try {
      //below code fetch opd history data
      let result = await patientDAO.retrievespecficOPDhistory(req.params.id);

      console.log(
        "result in retrieveOpdHistory api-----" + JSON.stringify(result)
      );
      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log(
        "err occured in retrieveOpdHistory index js due to : " + err
      );
      logger.error(
        "err occured in retrieveOpdHistory index js due to : " + err
      );
      callback(responseCreator(null, 500, false, err));
    }
  },

  /**
   * @author: Rambabu
   * @date:02-05-2021
   * @description: sendOPDEprescriptionmail
   */
  OPDEprescriptionmail: async (mailjson) => {
    let final_result = null;
    console.log("Entered sendOPDEprescriptionmail function...." + JSON.stringify(mailjson));
    try {
      let html = await opdEprescriptionmail.opdeprescriptionEmailsTemp(mailjson);
      var options = {
        format: "A3",
        orientation: "landscape",
      };
      await pdf.create(html, options).toBuffer(async (err, buffer) => {
        if (err) {
          return console.log("error");
        }
        let mail_id =
          mailjson.hospital_email != null
            ? mailjson.hospital_email
            : "sjakkina@gmail.com";
          await emailService
          .sendMail(
            mail_id,
            "ePrescription OPD",
            "Please find your requested attachment",
            [
              {
                filename: "ePrescriptionOPD.pdf",
                content: buffer,
                contentType: "application/pdf",
                contentDisposition: "attachment",
              },
            ]
          )
          .then((sendEmailRes) => {
            if (sendEmailRes.status === "success") {
              final_result = responseCreator("Mail send successfully!!!", 200, true, null);
              return final_result;
            } else {
              final_result = responseCreator("Mail not send successfully", 200, true, null);
              return final_result;
            }
          });
      });
      final_result = responseCreator("Mail send successfully", 200, true, null);
      return final_result;
     
    } catch (err) {
      console.log(
        "err occured in send OPDEprescription mail function due to : " + err
      );
      final_result = responseCreator(
        null,
        200,
        false,
        "Mail not send successfully"
      );
      return final_result;
    }
  },
};

module.exports = PatientService;
