/**
 * @description: This Service class will do Hospital  CRUD operations
 * @author: Rambabu
 * @date :20-09-2020
 */

const JSON = require("circular-json");
const { isNotNull, isArrayNotEmpty } = require("../../utils/validators");
const { responseCreator } = require("../../utils/responsehandler");
const commonDAO = require("../../dao/commonDAO");

var CommonServices = {
  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:01-10-2020
   * @description: Hospital Creation
   */
  fetchpanameSearch: async function (req, callback) {
    console.log("Entered fetchpanameSearch  service" + JSON.stringify(req.body));
    try {     

      let result = await commonDAO.panameSearch(req.body);
      console.log("result in fetchpanameSearch api" + JSON.stringify(result));

      if (isNotNull(result) && isArrayNotEmpty(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(
          responseCreator("Data not avaliable...", 500, false, null)
        );
      }
    } catch (err) {
      console.log("err occured in create hospital index js due to : " + err);
      logger.error("err occured in create hospital index js due to : " + err);
      callback(responseCreator(null, 500, false, err));
    }
  },
  
};



module.exports = CommonServices;
