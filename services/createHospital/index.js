/**
 * @description: This Service class will do Hospital  CRUD operations
 * @author: Rambabu
 * @date :20-09-2020
 */

const JSON = require("circular-json");
const { isNotNull, isArrayNotEmpty } = require("../../utils/validators");
const { responseCreator } = require("../../utils/responsehandler");
const hospitalDAO = require("../../dao/hospitalDAO");
var hospital = require("../../models/hospital");
const bedsDAO = require("../../dao/bedsDAO");
const patientDAO = require("../../dao/patientDAO");
const userDAO = require("../../dao/userDAO");
const mongoose = require("mongoose");
const momentTime = require("moment-timezone");
const moment = require("moment");
var HospitalService = {
  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:01-10-2020
   * @description: Hospital Creation
   */
  create: async function (req, callback) {
    console.log("Entered Hospital create  service" + JSON.stringify(req.body));
    try {
      let date = new Date();
      let curr_mon = date.getMonth();
      curr_mon = curr_mon + 1; //months start from 0
      let random_no = Math.floor(Math.random() * (999 - 100 + 1) + 100);
      let hcode = req.body.name.substring(0, 3); //first 3 letters
      let hospitalcode = hcode + curr_mon + random_no;

      console.log("hospitalcode is---------" + hospitalcode);
      req.body.code = hospitalcode; //adding hospital code
      let result = await hospitalDAO.create(req.body);
      console.log("result in create hospital api" + JSON.stringify(result));

      if (isNotNull(result)) {
        callback(responseCreator("Saved Successfully...", 200, true, null));
      } else {
        callback(
          responseCreator("Not saved successfully...", 500, false, null)
        );
      }
    } catch (err) {
      console.log("err occured in create hospital index js due to : " + err);
      logger.error("err occured in create hospital index js due to : " + err);
      callback(responseCreator(null, 500, false, err));
    }
  },

  /**
   * @param {*} query
   * @param {*} callback
   * @author: Rambabu
   * @date:01-10-2020
   * @description: hospital retrieve data
   */

  retrieve: async function (req, callback) {
    console.log("Entered Hospital retrieve api call");

    try {
      let result = await hospitalDAO.retrieve();

      console.log("result in retrieve hospital api" + JSON.stringify(result));

      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data is not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in retrieve hospital index js due to : " + err);
      logger.error("err occured in retrieve hospital index js due to : " + err);
      callback(responseCreator("Data not avaliable...", 500, false, err));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:02-10-2020
   * @description: hospital update data
   */

  update: async function (req, callback) {
    console.log("Entered Hospital update api call");
    try {
      const filter = { _id: req.params.id.toString() };
      delete req.body._id;
      
      let result = await hospitalDAO.update(filter, req.body);

      console.log("result in retrieve hospital api" + JSON.stringify(result));

      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data is not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in retrieve hospital index js due to : " + err);
      logger.error("err occured in retrieve hospital index js due to : " + err);
      callback(responseCreator("Data not avaliable...", 500, false, err));
    }
  },

  /**
   * @param {*} query
   * @param {*} callback
   * @author: Rambabu
   * @date:06-10-2020
   * @description: hospital fetchHospital data state wise
   */

  fetchHospital: async function (req, callback) {
    console.log("Entered fetchHospital retrieve api call");

    try {
      let result = await hospitalDAO.fetchHospital();
      let group_result = "";
      let resu_ = "";

      if (isNotNull(result)) {
        // Group by json based on state
        group_result = groupbyjson(result, "state");

        //constructing json based on state group json
        resu_ = await constructingJson(group_result);
      }

      console.log("result in fetchHospital api----" + JSON.stringify(resu_));

      if (isNotNull(resu_)) {
        callback(responseCreator(resu_, 200, true, null));
      } else {
        callback(responseCreator("Data is not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in fetchHospital index js due to : " + err);
      logger.error("err occured in fetchHospital index js due to : " + err);
      callback(responseCreator("Data not avaliable...", 500, false, err));
    }
  },
  getStateHospital: async function (req, callback) {
    console.log("Entered getStateHospital start ======= "+ new Date() );

    try {
      const loggedinUserId = req.params.userId;
      let hosptilIds =null;
      let result = null;

      if(loggedinUserId){
        let userData =  await userDAO.getUserForId(loggedinUserId);

        if(userData.hosptIds && userData.hosptIds.length >0){
         hosptilIds = userData.hosptIds;
        }
      }
    if(hosptilIds){
      result = await hospitalDAO.fetchHospitalIds(hosptilIds);
    }else{
      result = await hospitalDAO.fetchHospital();
    }

      //result = await hospitalDAO.fetchHospital();
      let group_result = "";
      let resu_ = "";

      if (isNotNull(result)) {
        // Group by json based on state
        group_result = groupbyjson(result, "state");

        //constructing json based on state group json
        resu_ = await constructingMenuJson(group_result);
      }

      console.log("Entered getStateHospital end =======" + new Date() + " : res "+ JSON.stringify(resu_));

      if (isNotNull(resu_)) {
        callback(responseCreator(resu_, 200, true, null));
      } else {
        callback(responseCreator("Data is not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in getStateHospital index js due to : " + err);
      logger.error("err occured in getStateHospital index js due to : " + err);
      callback(responseCreator("Data not avaliable...", 500, false, err));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:08-10-2020
   * @description: Fetch pateint details
   */
  fetchbed: async function (req, callback) {
    console.log("Entered fetchbed  service" + JSON.stringify(req.body));
    try {
      let result = await bedsDAO.getBedForHospitalId(
        req.body.hid,
        req.body.bed_no
      );
      //let result = await bedsDAO.getBedForHospitalId('5f781529bb23292a50f12920', 1);

      console.log("result in fetchbed api---" + JSON.stringify(result));

      if (result) {
        let patent_id = result.fk_patient_id;

        let patint_result = await patientDAO.getpatientDetails(patent_id);
        if(patint_result && patint_result._id){
          let history = await patientDAO.fetchOpdHistory(patent_id);
          history.forEach((his) => {
            var localTime = momentTime
        .tz(his.createdDate, "Asia/Kolkata")
        .format();
        his.createdDate = moment(localTime).format('YYYY-MM-DD HH:mm');;
          });
          patint_result.opdHistory = history;
        }else{
          patint_result.opdHistory = [];
        }

        console.log("Patient result is---" + JSON.stringify(patint_result));

        callback(responseCreator(patint_result, 200, true, null));
      } else {
        callback(responseCreator("Data is Not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in fetchbed index js due to : " + err);
      logger.error("err occured in fetchbed index js due to : " + err);
      callback(responseCreator(null, 500, false, err));
    }
  },
  getPatientMRNums: async function (req, callback) {
    console.log("Entered getPatientMRNums  service" + JSON.stringify(req.body));
    let patientMRNums =[];
    try {
      let result = await bedsDAO.getAllBedsForHospitalId(
        req.body.hospId,
      );
      console.log("result in fetchbed api---" + JSON.stringify(result));
  
      if (result) {
        let paitentIds =[];
        
        result.forEach((bedpat) => {
          paitentIds.push(mongoose.Types.ObjectId(bedpat.fk_patient_id));
        });
  
        let patint_result = await patientDAO.getPatientsForIds(paitentIds);

        patint_result.forEach((patient) => {
          patientMRNums.push(patient.mrNum);
        });
  
        console.log("Patient result is---" + JSON.stringify(patint_result));
        if(patientMRNums.length >0){
          callback(responseCreator(patientMRNums, 200, false, null));
        }else{
          callback(responseCreator(patientMRNums, 200, true, null));
        }
        
      } else {
        callback(responseCreator("Data is Not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in getPatientMRNums index js due to : " + err);
      logger.error("err occured in getPatientMRNums index js due to : " + err);
      callback(responseCreator(null, 500, false, err));
    }
  },
};

/**
 * @description:Group by json
 * @author:Rambabu
 * @date:05-10-2020
 * @param {*} xs
 * @param {*} key
 */
function groupbyjson(xs, key) {
  return xs.reduce(function (rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
}

/**
 * @description:Constructing JSON
 * @author:Rambabu
 * @date:06-10-2020
 * @param {*} xs
 * @param {*} key
 */
async function constructingJson(arr) {
  //console.log('group results in constructingJson ke is------' + JSON.stringify(arr));
  let states = [];
  for (const [key, value] of Object.entries(arr)) {
    let stats = {};
    let cityArry = [];
    stats.stateName = key;

    let city_group = groupbyjson(value, "city");
    for (const [citykey, cityvalue] of Object.entries(city_group)) {
      let city = {};
      let hosptArry = [];
      city.cityName = citykey;
      let city_hos_group = groupbyjson(cityvalue, "name");
      for (const [hospkey, hospvalue] of Object.entries(city_hos_group)) {
        let hospital = {};
        hospital.name = hospkey;
        hospital.code = hospvalue[0].code;
        hospital.id = hospvalue[0]._id.toString();
        hosptArry.push(hospital);
      }
      city.hospitals = hosptArry;
      cityArry.push(city);
    }
    stats.cities = cityArry;
    states.push(stats);
  }
  console.log(JSON.stringify(states));

  for (let d in arr) {
    let data = arr[d];
    console.log("constructingJson ke is------" + d);

    //group by city
    let city_group = groupbyjson(data, "city");

    //console.log('city_group results in constructingJson ke is------' + JSON.stringify(city_group));

    for (let c in city_group) {
      let city_d = city_group[c];

      //Group by hospital name under city
      let city_hos_group = groupbyjson(city_d, "name");
      console.log("city_hos_group data------" + JSON.stringify(city_hos_group));

      for (let ch in city_hos_group) {
        let hos_d = city_hos_group[ch];
        let hosp_arra = [];

        let bed_obj = [];
        //constructing bed
        for (var i = 1; i <= hos_d[0].noofbeds; i++) {
          //fetching the bed deatisl like status

          let result = await bedsDAO.getBedForHospitalId(hos_d[0]._id, i);
          let resBed = [];
          if(result && isNotNull(result.fk_patient_id)){
            resBed = await bedsDAO.getPatientForBed(hos_d[0]._id.toString(), i);
          }

          //console.log('getBedForHospitalId result-------'+JSON.stringify(testRes))
          let bed_d = {
            bed_id: i,
            name: "",
            fk_hosp_id: hos_d[0]._id.toString(),
            status: "vacant",
          }; 
          if(resBed.length >0){
            bed_d.name = resBed[0].name;
            bed_d.status = resBed[0].status;
          }
          bed_obj.push(bed_d);
        }
        const hospi_onj = {
          hospital_id: hos_d[0]._id,
          hcode: hos_d[0].code,
          hname: hos_d[0].name,
          email: hos_d[0].email,
          bed_details: bed_obj,
        };
        hosp_arra.push(hospi_onj);
        city_hos_group[ch] = hosp_arra;
      }
      city_group[c] = city_hos_group;
    }
    arr[d] = city_group;
  }
  //console.log('Final constructing json is------' + JSON.stringify(arr));
  return arr;
}

async function constructingMenuJson(arr) {
  //console.log('group results in constructingJson ke is------' + JSON.stringify(arr));
  let states = [];
  for (const [key, value] of Object.entries(arr)) {
    let stats = {};
    let cityArry = [];
    stats.stateName = key;

    let city_group = groupbyjson(value, "city");
    let bedsData = await bedsDAO.fetchBeds();
    let patientsIds = [];

    bedsData.forEach((bedpat) => {
      patientsIds.push(mongoose.Types.ObjectId(bedpat.fk_patient_id));
    });
    let patients = await patientDAO.getPatientsForIds(patientsIds);

    for (const [citykey, cityvalue] of Object.entries(city_group)) {
      let city = {};
      let hosptArry = [];
      city.cityName = citykey;
      let city_hos_group = groupbyjson(cityvalue, "name");
      for (const [hospkey, hospvalue] of Object.entries(city_hos_group)) {
        let hospital = {};
        hospital.name = hospkey;
        hospital.code = hospvalue[0].code;
        hospital.id = hospvalue[0]._id.toString();
        hospital.email = hospvalue[0].email;
        //constructing bed
        let bed_obj = [];
        for (var i = 1; i <= hospvalue[0].noofbeds; i++) {
          //fetching the bed deatisl like status
          const bedsSel = bedsData.filter(
            (beds) =>
            beds.fk_hosp_id ===
            hospvalue[0]._id.toString() && beds.bed_Num ===i
          );
          let patientSel = null;
          if(bedsSel && bedsSel.length > 0){
            
             patientSel = patients.filter(
              (selPatent) =>
              bedsSel[0]._id.toString() === selPatent.bed_id
            );
            
          }
          //let result = await bedsDAO.getBedForHospitalId(hospvalue[0]._id, i);
          //let resBed = [];
          //if(result && isNotNull(result.fk_patient_id)){
            //resBed = await bedsDAO.getPatientForBed(hospvalue[0]._id.toString(), i);
          //}
          //console.log('getBedForHospitalId result-------'+JSON.stringify(testRes))
          let bed_d = {
            bed_id: i,
            name: "",
            fk_hosp_id: hospvalue[0]._id.toString(),
            status: "vacant",
          }; 
          if(patientSel && patientSel.length >0){
            bed_d.name = patientSel[0].name;
            bed_d.status = bedsSel[0].status;
          }

          
          bed_obj.push(bed_d);
        }
        hospital.bed_details = bed_obj;
        hosptArry.push(hospital);
      }
      city.hospitals = hosptArry;
      cityArry.push(city);
    }
    stats.cities = cityArry;
    states.push(stats);
  }
  console.log(JSON.stringify(states));

  //console.log('Final constructing json is------' + JSON.stringify(arr));
  return states;
}



module.exports = HospitalService;
