/**
 * @description: This Service class will do Hospital  CRUD operations
 * @author: Rambabu
 * @date :20-09-2020
 */

const JSON = require("circular-json");
const { isNotNull, isArrayNotEmpty } = require("../../utils/validators");
const { responseCreator } = require("../../utils/responsehandler");
const randomstring = require("randomstring");
const userDAO = require("../../dao/userDAO");
const roleDAO = require("../../dao/roleDAO");
const emailService = require("../email");
const forgotPasswordEmail = require("../../emailTemplates/forgotPasswordEmail");
const template = require("../../emailTemplates/template");

var LoginService = {
  /**
   * @package services\branch
   * @param {*} query
   * @param {*} callback
   * @author: Rambabu
   * @date:2020
   * @description: Branch Creation
   */
  login: async function (req, callback) {
    let final_result = null;
    console.log("Entered login create  service" + JSON.stringify(req.body));
    try {
      let userData ={};
      let user = await userDAO.validateUser(req.body.userName);
      console.log("user login" + JSON.stringify(user));
      if (user) {
        if (!user.isActive) {
          final_result = responseCreator(
            null,
            200,
            false,
            "User disabled. Please contact Customer Support Team."
          );
        }
        let validPwd = await user.isPasswordValid(req.body.password);      

        console.log('validPwd--------'+JSON.stringify(validPwd));
        if (validPwd === true) {
          userData.isActive =user.isActive;
          userData._id = user._id;
          
          userData.email = user.email;
          userData.firstName = user.firstName;
          userData.lastName = user.lastName;
          userData.userName = user.userName;
          userData.isVerified = user.isVerified;
            if(user.roleId){
              const roles = await roleDAO.getRoleById(user.roleId);
              if(roles){
                userData.permissions = roles.permissions;
              }
            }
          

          final_result = responseCreator(userData, 200, true, null);
        } else {
          final_result = responseCreator(
            null,
            200,
            false,
            "Invalid username and password"
          );
        }
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Invalid username and password"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in login due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Invalid username and password"
      );
      callback(final_result);
    }
  },

  sendForgotPasswordEmail: async function (req, callback) {
    console.log(
      "Entered sendForgotPasswordEmail create  service" +
        JSON.stringify(req.body)
    );
    let final_result = null;

    let emailData = {};
    let token = randomstring.generate(7);
    console.log("Token " + JSON.stringify(token));
    emailData.password = token;

    let user = await userDAO.getUserForEmail(req.body.email);
    emailData.name = user.firstName + " "+ user.lastName;
    console.log("User data " + JSON.stringify(user));
    user.password = user.generateHash(emailData.password);
    let updateReq = {
      password: user.password,
      modifiedDate: new Date(),
    };
    const filter = { _id: user._id.toString() };
    let resp = await userDAO.update(filter, updateReq);
    console.log("Response " + JSON.stringify(resp));
    if (resp) {
      const html = await forgotPasswordEmail.forgotPasswordEmail(emailData);
      const templates = await template.template(html);

      await emailService.sendMail(req.body.email, "Forgot Password", templates);
      final_result = responseCreator(
        null,
        200,
        true,
        "send email successfully"
      );
    } else {
      final_result = responseCreator(null, 200, true, "send email failed");
    }

    callback(final_result);
  },
  changePassword: async function (req, callback) {
    console.log(
      "Entered changePassword create  service" +
        JSON.stringify(req.body)
    );
    let final_result = null;

    let user = await userDAO.getUserForId(req.body.usrId);
    console.log("User data " + JSON.stringify(user));
    if(user){
      let validPwd = await user.isPasswordValid(req.body.oldPwd);
    if (!validPwd) {
      final_result = responseCreator(null, 200, false, "Invalid Password");
    }else{
      user.password = await user.generateHash(req.body.newPwd);
      let updateReq = {
        password: user.password,
        isVerified: true,
        modifiedDate: new Date(),
      };
      const filter = { _id: user._id.toString() };
      let resp = await userDAO.update(filter, updateReq);
      console.log("Response " + JSON.stringify(resp));
      if (resp) {
        
        final_result = responseCreator(
          null,
          200,
          true,
          "Password Updated Successfully"
        );
      } else {
        final_result = responseCreator(null, 200, false, "Password Not Updated");
      }
    }
    }else{
      final_result = responseCreator(null, 200, false, "Password Not Updated");
    }
    callback(final_result);
  },
};

module.exports = LoginService;
