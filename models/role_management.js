const mongoose = require("mongoose");

const roleManagementSchema = mongoose.Schema({
  roleName: {
    type: String,
    required: true,
  },
  desc: {
    type: String,
    required: true,
  },
  permissions: {
    type: [String],
  },
  isActive: {
    type: Boolean,
    default: true,
    required: false,
  },
  createdBy: {
    type: String,
    required: false,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  modifiedBy: {
    type: String,
    required: false,
  },
  modifiedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
});
const roleManagement = mongoose.model("roles", roleManagementSchema, "roles");
module.exports = roleManagement;
