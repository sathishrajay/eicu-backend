const mongoose = require("mongoose");

const BedsSchema = mongoose.Schema({
  bed_Num: {
    type: Number,
    required: false,
  },
  fk_hosp_id: {
    type: String,
    required: false,
  },
  fk_patient_id: {
    type: String,
    required: false,
  },
  status: {
    type: String,
    required: false,
  },
  createdBy: {
    type: String,
    required: false,
  },
  modifiedBy: {
    type: String,
    required: false,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  modifiedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
});

const Beds = mongoose.model("Beds", BedsSchema, "Beds");
module.exports = Beds;
