const mongoose = require("mongoose");

const spocDetailsSchema = mongoose.Schema({
  patientId: {
    type: String,
    required: true,
  },
  spoc: {
    type: String,
    required: true,
  },
  createdBy: {
    type: String,
    required: false,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  
});
const spocDetails = mongoose.model("spocDetails", spocDetailsSchema, "spocDetails");
module.exports = spocDetails;
