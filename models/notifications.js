const mongoose = require("mongoose");

const NotificationSchema = mongoose.Schema({
    notify_ref: {
        type: String,
        required: true,
    },
    ptient_name:{
        type: String,
        required: false,
    },
    notify_type: {
        type: String,
        required: true,
    },
    priority: {
        type: String,
        required: true,
    },
    notify_landing: {
        type: String,
        required: false,
    },
    notify_ref_type: {
        type: String,
        required: false,
    },
    message: [
        {
            title: {
                type: String,
                required: false,
            },
            titleAr: {
                type: String,
                required: false,
            },
            description: {
                type: String,
                required: false,
            },
            descriptionAr: {
                type: String,
                required: false,
            }
        }
    ],
    fk_user_id: {
        type: String,
        required: true,
    },
    isActive: {
        type: Boolean,
    },
    readStatus: {
        type: Boolean,
    },
    createdDate: {
        type: Date,
        default: Date.now,
        required: false,
    }
});

const NotificationModel = mongoose.model("Notifications", NotificationSchema);
module.exports = NotificationModel;
