const winston = require('winston');
const moment = require('moment');

const appSettings = {
    winston: {
        sillyLogConfig: {
            level: 'silly',
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.json(),
                winston.format.printf(info => {
                    const dateObj=(`time`, moment().format('YYYY-MM-DD HH:mm:ss.SSS'));
                    return `${dateObj} ${info.level.toUpperCase()} : ${info.message}`
                    //DD/MMM/YYYY:HH:mm:ss
                })
            ),
            transports: [
                new winston.transports.File({
                    filename: './logs/eicu.log'
                }),
                new winston.transports.Console()
            ]
        },
    },
   
};

module.exports = appSettings;